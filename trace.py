#!/usr/bin/env python3

import os
import pyshark
from argparse import ArgumentParser


def trace(time, interface, output_file='trace.pcap'):
    """
    Fonction de capture de paquets
    """
    capture = pyshark.LiveCapture(interface=interface, display_filter="not dns and not mdns and not igmp", output_file=output_file)
    print("\n-----------------")
    print("Début de la capture")
    print("Sur:")
    print(capture.interfaces)
    print("-----------------\n")
    capture.sniff(timeout=int(time))
    print(capture)
    capture.close()
    print("\nFin de la capture\n")

    #pcapfix pour réparer la trace si besoin
    cmd = 'sudo pcapfix ' + output_file
    os.system(cmd)

def main():
    parser = ArgumentParser()
    parser.add_argument('--time', '-t', help='Temps en seconde de la capture')
    parser.add_argument('--interface', '-i', nargs='*', help='Interface à écouter')
    parser.add_argument('--output', '-o', help='Nom du fichier de la capture')

    args = parser.parse_args()

    if args.time is not None:
        trace(int(args.time), args.interface, args.output)
    else:
        parser.print_help()

if __name__ == '__main__':
    main()