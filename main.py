#!/usr/bin/env python3

import trace
import attack
import threading
import random
import time

from argparse import ArgumentParser

def main():
    """ 
    Fonction permettant de lancer les script attack et trace de manière simultanée 
    """
    parser = ArgumentParser()
    parser.add_argument('-ip', help='ip à attaquer')
    parser.add_argument('-port', '-p', help='port à attaquer')
    parser.add_argument('--time', '-t', help='Temps en seconde de la capture')
    parser.add_argument('--nb_atk', '-nb', help='Nombre d\'attaque à effectuer pendant la capture')
    parser.add_argument('--interface', '-i', nargs='*', help='Interface à écouter (ajouter espace si plusieurs interfaces ex: \"--interface wlp2s0 lo\")')
    parser.add_argument('--output', '-o', help='Nom du fichier de la capture')
    parser.add_argument('--seed', '-s', help='seed a rejouer (permet de rejouer un scénario précis d\'attaque)')
    parser.add_argument("--verbose", '-v', action='store_true', help="Mode verbose")
    parser.add_argument("--full", '-f', action='store_true', help="Capture avec chaque attaque tirée entre 1 et 3 fois (L'argument \"--nb_atk\" n'a aucun impact si \"--full\" est activé)")

    args = parser.parse_args()

    if args.ip is not None:
        print("---------------------------------------")
        if args.seed is not None:
            seed = args.seed
            random.seed(int(args.seed))
            print("SEED REJOUE:", seed)
        else:
            seed = int(time.time())
            random.seed(seed)
            print("SEED ALEATOIRE:", seed)
        print("---------------------------------------\n")
        

        verbose=False
        if args.verbose:
            print("---------------------------------------")
            print("Verbose activé")
            verbose=True
            print("---------------------------------------\n")

        if args.nb_atk is None:
            nb_atk = 0
        else:
            nb_atk = int(args.nb_atk)
        
        threading.Thread( target=attack.random_attack, args=( nb_atk, args.ip, int(args.port), int(args.time)*1000, args.interface, verbose, seed, args.output, args.full ) ).start()
        threading.Thread( target=trace.trace, args=( int(args.time), args.interface, args.output ) ).start()
    else:
        parser.print_help()

if __name__ == '__main__':
    main()