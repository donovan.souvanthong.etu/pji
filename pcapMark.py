#!/usr/bin/env python3
import os
import pyshark
import utils
from argparse import ArgumentParser

def marquage(filtre, pcap, attack, option):
    """ 
    Fonction permettant de marquer les fichiers pcap 
    """
    # sauvegarde pcap
    cmd = 'cp ' + pcap + " " + pcap +".bak"
    os.system(cmd)

    ##### Variable pour fichier attack
    attack_paquet = []
    attack_nb_paquet_ddos = 0

    ##### Variable pour fichier filtre
    filtre_nb_paquet_ddos = 0
    filtre_paquet_ddos = []

    ######################################### LECTURE FICHIER ATTACK ######################################### 
    attack_paquet, attack_nb_paquet_ddos = utils.read_attack_file(attack)

    ######################################### LECTURE FICHIER FILTRE #########################################
    reader = utils.ids_reader(filtre)
    filtre_nb_paquet_ddos, filtre_paquet_ddos = reader.read()

    ######################################### LECTURE FICHIER PCAP #########################################
    shark_cap = pyshark.FileCapture(pcap)

    if(option == "full"):
        commande = "editcap"
        num_paquet = 0
        num_marque = 0
        for packet in shark_cap:
            num_paquet += 1       
            src_ip, dst_ip, src_port, dst_port = utils.get_packet_info(packet)

            commande += " -a " + str(num_paquet) + ":\""

            if(len(attack_paquet) > 0):
                fst_packet = attack_paquet[0]
                if(src_ip == fst_packet[0] and src_port == fst_packet[1] and dst_ip == fst_packet[2] and dst_port == fst_packet[3]):
                    attack_paquet.pop(0)
                    commande += "Paquet Attaquant "
                else:
                    commande += "Paquet Legit "
            else:
                commande += "Paquet Legit "
            
            if(num_paquet in filtre_paquet_ddos):
                commande += "Marqué Attaquant" 
            else:
                commande += "Marqué Legit"
        
            commande += "\""

            if(num_paquet%500 == 0):
                print("Marquage paquet: " + str(num_marque) + " à " + str(num_paquet))
                num_marque = num_paquet
                commande += " " + pcap + " temp.pcapng"
                os.system(commande)
                os.system("mv temp.pcapng " + pcap)
                commande = "editcap"

        if(num_marque != num_paquet):
            print("Marquage paquet: " + str(num_marque) + " à " + str(num_paquet))
            commande += " " + pcap + " temp.pcapng"
            os.system(commande)
            os.system("mv temp.pcapng " + pcap)
            
    elif(option == "attack"):
        commande = ""
        num_paquet = 0
        packets_ddos = []
        for packet in shark_cap:
            num_paquet += 1        
            src_ip, dst_ip, src_port, dst_port = utils.get_packet_info(packet)

            if(len(attack_paquet) > 0):
                fst_packet = attack_paquet[0]
                if(src_ip == fst_packet[0] and src_port == fst_packet[1] and dst_ip == fst_packet[2] and dst_port == fst_packet[3]):
                    attack_paquet.pop(0)
                    commande += "Emitted DDoS "
            
            if(num_paquet in filtre_paquet_ddos):
                commande += "Detected DDoS" 

            if(commande != ""):
                packets_ddos.append((num_paquet, commande))
                commande = ""
        
        num_marque = 0
        commande = "editcap"
        for ligne, commentaire in packets_ddos:
            num_marque += 1
            commande += " -a " + str(ligne) + ":\"" + commentaire + "\""
            if(num_marque%500 == 0):
                print("Marquage paquet: " + str(ligne))
                commande += " " + pcap + " temp.pcapng"
                os.system(commande)
                os.system("mv temp.pcapng " + pcap)
                commande = "editcap"

        if(commande != "editcap"):
            print("Marquage paquet: " + str(ligne))
            commande += " " + pcap + " temp.pcapng"
            os.system(commande)
            os.system("mv temp.pcapng " + pcap)


def main():
    parser = ArgumentParser()
    parser.add_argument('--attack', help='Fichier attaque de la capture')
    parser.add_argument('--filtre', help='Fichier résultant du filtre mis en place')
    parser.add_argument('--pcap', help='Fichier pcap de la capture')

    args = parser.parse_args()
    
    if args.filtre is not None and args.pcap is not None: 
        print("\nMarquage du pcap veuillez patienter")
        print("-----------------------------------------------------")
        marquage(args.filtre, args.pcap, args.attack, "attack")
        print("-----------------------------------------------------")
        print("Fin marquage")
    else:
        parser.print_help()


if __name__ == '__main__':
    main()