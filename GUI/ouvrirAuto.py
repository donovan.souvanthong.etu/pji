from tkinter import * 
from tkinter import ttk
from tkinter import messagebox

import GUI.ouvrirManuel as ouvrirManuel
import GUI.result as result
import GUI.calcul as calcul
import configparser
import os
import utils

class ouvrirAuto(object): 
    def __init__(self, parent, app): 
        # Fenêtre d'ouverture des fichiers via un dossier
        # Elle permet d'ouvrir les fenêtre benchmark (calcul.py) et d'ouvrir des captures existantes (result.py)
        folder = StringVar()
        pcap = StringVar()
        log_atk = StringVar()
        log_info = StringVar()
        result_json = StringVar()
        result_log = StringVar()
        filtre = StringVar()

        frm = ttk.Frame(parent, padding=10)
        frm.grid()

        if(app == "Benchmark"):
            label_app = "Calcul du score de la trace"
            title_app = app
            label_button = "Valider Benchmark"
        elif (app == "trace"):
            label_app = "Ouvrir " + app
            title_app = "Ouverture de " + app
            label_button = "Valider Trace"    

        ttk.Label(frm, text=label_app).grid(column=0, columnspan=5, row=0)

        ttk.Button(frm, text="Lien Dossier", command=lambda: utils.select_folder(folder)).grid(column=0, row=1)
        ttk.Button(frm, text="Saisie manuel", command=lambda: ouvrirAuto.call_manuel(self, app, folder.get())).grid(column=1, row=1)
        ttk.Entry(frm, textvariable=folder).grid(column=0, row=2)
        # ttk.Button(frm, text="Retour", command=lambda: utils.on_return(self, parent)).grid(column=0, row=3)
        ttk.Button(frm, text=label_button, command=lambda: ouvrirAuto.auto_open(self, folder, pcap, log_atk, log_info, result_json, result_log, filtre, app)).grid(column=1, row=2) 

        self.root2 = parent 
        self.root2.title(title_app) 
        self.root2.protocol("WM_DELETE_WINDOW", lambda: utils.on_closing(self))
        self.root2.resizable(False, False)

    def openFrame(self):
        pass

    def call_manuel(self, app, folder):
        self.root2.withdraw()
        new_window = Toplevel()
        ouvrirManuel.ouvrirManuel(new_window, app, folder)

    def auto_open(self, folder, pcap, log_atk, log_info, result_json, result_log, filtre, app):
        config = configparser.ConfigParser()
        config.sections()
        config.read('./GUI/default.conf')
        ################ Section du fichier default.conf a lire ################
        section = "CUSTOM"
        ########################################################################
        pcap.set(folder.get()+"/"+config[section]['Pcap'])
        filtre.set(folder.get()+"/"+config[section]['Filtre'])
        log_atk.set(folder.get()+"/"+config[section]['Log_atk'])
        log_info.set(folder.get()+"/"+config[section]['Log_info'])
        result_json.set(folder.get()+"/"+config[section]['Result_json'])
        result_log.set(folder.get()+"/"+config[section]['Result_log'])

        if not os.path.exists(pcap.get()) or \
        not os.path.exists(filtre.get()) or \
        not os.path.exists(log_atk.get()) or \
        not os.path.exists(log_info.get()) or \
        not os.path.exists(result_json.get()) or \
        not os.path.exists(result_log.get()):
            messagebox.showinfo("Fichier manquant", "Erreur: un ou plusieurs fichiers n'a pas été trouvé (Voir default.conf pour le format des fichiers)")
        else:
            if(app == "trace"):
                ouvrirAuto.call_result(self, pcap.get(), log_atk.get(), log_info.get(), result_json.get(), result_log.get(), filtre.get())
            elif(app == "Benchmark"):
                ouvrirAuto.call_calcul(self, pcap.get(), log_atk.get(), log_info.get(), filtre.get())

    def call_result(self, pcap, log_atk, log_info, result_json, result_log, filtre):
        self.root2.withdraw()
        new_window = Toplevel()
        result.open_capture(new_window, pcap, log_atk, log_info, result_json, result_log, filtre)

    def call_calcul(self, pcap, log_atk, log_info, filtre):
        self.root2.withdraw()
        new_window = Toplevel()
        calcul.Calcul(new_window, pcap, log_atk, log_info, filtre)
