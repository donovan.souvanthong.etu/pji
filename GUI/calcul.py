from tkinter import * 
from tkinter import ttk

import sys
import subprocess
import utils

class Calcul(object):
    def __init__(self, parent, pcap, log_atk, log_info, filtre): 
        # Fenêtre calcul du benchmark
        frm = ttk.Frame(parent, padding=10)
        frm.grid()
        
        label_calcul = StringVar()
        label_calcul.set("Cliquez sur le bouton pour démarrer le benchmark")
        ttk.Label(frm, textvariable=label_calcul).grid(column=0, columnspan=6, row=0)

        frame = ttk.Frame(frm)
        frame.grid(row=1, column=1)

        text = Text(frame)
        text.grid(row=0, column=0)

        scrollbar = ttk.Scrollbar(frame)
        scrollbar.grid(row=0, column=1, sticky='ns')

        text['yscrollcommand'] = scrollbar.set
        scrollbar['command'] = text.yview
        old_stdout = sys.stdout    
        sys.stdout = utils.Redirect(text)

        ttk.Button(frm, text='Début du calcul', command=lambda: [label_calcul.set("Calcul en cours"), Calcul.run(pcap, log_atk, log_info, filtre, label_calcul, text)] ).grid(column=1, row=3)
        
        self.root2 = parent 
        self.root2.title("Calcul du score") 
        self.root2.protocol("WM_DELETE_WINDOW", lambda: utils.on_closing_std(self, sys.stdout))
        self.root2.resizable(False, False)
        self.root2.update_idletasks()

    def openFrame(self):
        pass

    def run(pcap, log_atk, log_info, filtre, label_calcul, text):
        # Permet de lancer la commande benchmark
        print("Start benchmark")

        output_table = []
        commande = "python3 bench.py --log " + log_info + " --filtre " + filtre + " --pcap " + pcap + " --attack " + log_atk + " --verbose"
        print(commande)
        p = subprocess.Popen(commande.split(), stdout=subprocess.PIPE, bufsize=1, text=True)
        while p.poll() is None:
            msg = p.stdout.readline().strip()
            if msg:
                # Afficher seulement 30 lignes dans text
                if(len(output_table) > 30):
                    text.delete('1.0', END)
                    for x in output_table[-30:]:
                        text.insert(END, x + '\n')
                    output_table.clear()
                output_table.append(msg)
                
                # Le print aurait pu marcher grâce a la clase utils.Redirect 
                # print(msg)
                # insert the line in the Text widget
                # force widget to display the end of the text (follow the input)
                text.see(END)
                # force refresh of the widget to be sure that thing are displayed
                text.update_idletasks()

        print("End benchmark")
        label_calcul.set("Fin du calcul")
        