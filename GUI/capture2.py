from tkinter import * 
from tkinter import ttk

import sys
import subprocess
import threading
import os
import utils

class Capture2(object):
    def __init__(self, parent, ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, output): 
        # Fenêtre de capture
        frm = ttk.Frame(parent, padding=10)
        frm.grid()
        
        label_capture = StringVar()
        label_capture.set("Cliquez sur le bouton pour démarrer la capture")
        ttk.Label(frm, textvariable=label_capture).grid(column=0, columnspan=6, row=0)

        ttk.Label(frm, text="IP:", anchor="e").grid(column=0, row=1, sticky=W)
        ttk.Label(frm, width=15, text=ip, anchor="e").grid(column=1, row=1, sticky=E)

        ttk.Label(frm, text="Port à attaquer:", anchor="e").grid(column=0, row=2, sticky=W)
        ttk.Label(frm, width=5, text=port, anchor="e").grid(column=1, row=2, sticky=E)

        ttk.Label(frm, text="interface à écouter:", anchor="e").grid(column=0, row=3, sticky=W)
        ttk.Label(frm, width=15, text=interface, anchor="e").grid(column=1, row=3, sticky=E)

        ttk.Label(frm, text="Temps de la capture (en secondes):", anchor="e").grid(column=0, row=4, sticky=W)
        ttk.Label(frm, text=temps, width=4, anchor="e").grid(column=1, row=4, sticky=E)

        ttk.Label(frm, text="Nombre d'attaque à effecuter:", anchor="e").grid(column=0, row=5, sticky=W)
        ttk.Label(frm, text=nbAttaque, width=2, anchor="e").grid(column=1, row=5, sticky=E)

        ttk.Label(frm, text="Nom de la capture:", anchor="e").grid(column=0, row=6, sticky=W)
        ttk.Label(frm, width=20, text=nomCapture, anchor="e").grid(column=1, row=6, sticky=E)

        ttk.Label(frm, text="Arguments optionnels:", anchor="e").grid(column=0, row=7)

        ttk.Label(frm, text="Seed à rejouer:", anchor="e").grid(column=0, row=8, sticky=W)
        if(seed == ""): label_seed = "Généré aléatoirement"
        else: label_seed = seed
        ttk.Label(frm, width=20, text=label_seed, anchor="e").grid(column=1, row=8, sticky=E)

        ttk.Label(frm, text="Mode Verbose:", anchor="e").grid(column=0, row=9, sticky=W)
        if(verbose == 1): label_verbose = "Oui"
        else: label_verbose = "Non"
        ttk.Label(frm, text=label_verbose, anchor="e").grid(column=1, row=9, sticky=E)

        ttk.Label(frm, text="Mode Full:", anchor="e").grid(column=0, row=10, sticky=W)
        if(full == 1): label_full = "Oui"
        else: label_full = "Non"
        ttk.Label(frm, text=label_full, anchor="e").grid(column=1, row=10, sticky=E)

        if(output == 0): 
            frame = ttk.Frame(frm)
            frame.grid(row=12, column=0, columnspan=2)

            text = Text(frame)
            text.grid(row=0, column=0)

            scrollbar = ttk.Scrollbar(frame)
            scrollbar.grid(row=0, column=1, sticky='ns')

            text['yscrollcommand'] = scrollbar.set
            scrollbar['command'] = text.yview
            old_stdout = sys.stdout    
            sys.stdout = utils.Redirect(text)

            ttk.Button(frm, text='Début de la Capture', command=lambda: [label_capture.set("Capture en cours"), Capture2.run(ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, label_capture, text)]).grid(column=1, row=13)
        else:
            ttk.Button(frm, text='Début de la Capture', command=lambda: [label_capture.set("Capture en cours"), Capture2.run_terminal(ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, label_capture)]).grid(column=1, row=13)


        self.root2 = parent 
        self.root2.title("Capture") 
        self.root2.protocol("WM_DELETE_WINDOW", lambda: utils.on_closing_std(self, sys.stdout))
        self.root2.resizable(False, False)
        self.root2.update_idletasks()

    def openFrame(self):
        pass

    # def thread(ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, label_capture, text):
    #     threading.Thread(target=Capture2.run(ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, label_capture, text)).start()

    def run_terminal(ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, label_capture):
        print("Start capture")

        # output_table = []
        commande = "sudo python3 main.py -ip " + ip + " -port " + port + " --interface " + interface + " --time " + temps + " --nb_atk " + nbAttaque 
        
        if(nomCapture != ""):
            commande += " --output " + nomCapture 

        if(seed != ""):
            commande += " --seed " + seed 

        if(verbose == 1):
            commande += " --verbose"

        if(full == 1):
            commande += " --full"

        print(commande)        
        os.system(commande)
        print("End capture")
        label_capture.set("Fin de capture")


    def run(ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, label_capture, text):
        # Permet de lancer la commande de capture de trame
        # os.system("cd Captures")
        print("Start capture")

        # output_table = []
        commande = "sudo python3 main.py -ip " + ip + " -port " + port + " --interface " + interface + " --time " + temps + " --nb_atk " + nbAttaque 
        
        if(nomCapture != ""):
            commande += " --output " + nomCapture 

        if(seed != ""):
            commande += " --seed " + seed 

        if(verbose == 1):
            commande += " --verbose"

        if(full == 1):
            commande += " --full"

        print(commande)
        p = subprocess.Popen(commande.split(), stdout=subprocess.PIPE, bufsize=1, text=True)
        while p.poll() is None:
            msg = p.stdout.readline().strip()
            if msg:
                # if(len(output_table) > 30):
                #     text.delete('1.0', END)
                #     for x in output_table[-30:]:
                #         text.insert(END, x + '\n')
                #     output_table.clear()
                # output_table.append(msg)
                print(msg)
                # insert the line in the Text widget
                # text.insert(END, msg)
                # force widget to display the end of the text (follow the input)
                text.see(END)
                # force refresh of the widget to be sure that thing are displayed
                text.update_idletasks()

        print("End capture")
        label_capture.set("Fin de capture")
