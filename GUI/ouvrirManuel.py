from tkinter import * 
from tkinter import ttk
from tkinter import messagebox

import GUI.result as result
import GUI.calcul as calcul
import configparser
import os
import utils

class ouvrirManuel(object): 
    def __init__(self, parent, app, folder):
        # Fenêtre d'ouverture des fichiers via des path pour chaque fichiers 
        # Elle permet d'ouvrir les fenêtre benchmark (calcul.py) et d'ouvrir des captures existantes (result.py)
        pcap = StringVar(value="PAS TROUVÉ")
        log_atk = StringVar(value="PAS TROUVÉ")
        log_info = StringVar(value="PAS TROUVÉ")
        result_json = StringVar(value="PAS TROUVÉ")
        result_log = StringVar(value="PAS TROUVÉ")
        filtre = StringVar(value="PAS TROUVÉ")

        if folder != "":
            config = configparser.ConfigParser()
            config.sections()
            config.read('./GUI/default.conf')
            
            ################ Section du fichier default.conf a lire ################
            section = "CUSTOM"
            ########################################################################
            if os.path.exists(folder+"/"+config[section]['Pcap']):        pcap.set(folder+"/"+config[section]['Pcap'])
            if os.path.exists(folder+"/"+config[section]['Filtre']):      filtre.set(folder+"/"+config[section]['Filtre'])
            if os.path.exists(folder+"/"+config[section]['Log_atk']):     log_atk.set(folder+"/"+config[section]['Log_atk'])
            if os.path.exists(folder+"/"+config[section]['Log_info']):    log_info.set(folder+"/"+config[section]['Log_info'])
            if os.path.exists(folder+"/"+config[section]['Result_json']): result_json.set(folder+"/"+config[section]['Result_json'])
            if os.path.exists(folder+"/"+config[section]['Result_log']):  result_log.set(folder+"/"+config[section]['Result_log'])

        frm = ttk.Frame(parent, padding=10)
        frm.grid()

        if(app == "Benchmark"):
            label_app = "Ouvrir fichier pour calcul"
            title_app = app
                
            ttk.Button(frm, text="Valider Benchmark", command=lambda: ouvrirManuel.call_calcul(self, pcap.get(), log_atk.get(), log_info.get(), filtre.get())).grid(column=1, row=8)

        elif(app == "trace"):
            label_app = "Ouvrir fichier de capture"
            title_app = "Ouverture de " + app
            ttk.Label(frm, text="Fichiers optionnels:").grid(column=0, row=4, sticky=W)

            ttk.Button(frm, text="Fichier résultat .json", command=lambda: utils.select_file(result_json)).grid(column=0, row=5, sticky=W)
            ttk.Label(frm, text="LIEN", textvariable=result_json).grid(column=1, row=5)

            ttk.Button(frm, text="Fichier résultat .log", command=lambda: utils.select_file(result_log)).grid(column=0, row=6, sticky=W)
            ttk.Label(frm, text="LIEN", textvariable=result_log).grid(column=1, row=6)

            ttk.Button(frm, text="Valider trace", command=lambda: ouvrirManuel.call_result(self, pcap.get(), log_atk.get(), log_info.get(), result_json.get(), result_log.get(), filtre.get())).grid(column=1, row=8)

        ttk.Label(frm, text=label_app).grid(column=0, columnspan=5, row=0)

        ttk.Button(frm, text="Fichier .pcap", command=lambda: utils.select_file(pcap)).grid(column=0, row=1, sticky=W)
        ttk.Label(frm, text="LIEN", textvariable=pcap).grid(column=1, row=1)

        ttk.Button(frm, text="Fichier log atk", command=lambda: utils.select_file(log_atk)).grid(column=0, row=2, sticky=W)
        ttk.Label(frm, text="LIEN", textvariable=log_atk).grid(column=1, row=2)

        ttk.Button(frm, text="Fichier log info", command=lambda: utils.select_file(log_info)).grid(column=0, row=3, sticky=W)
        ttk.Label(frm, text="LIEN", textvariable=log_info).grid(column=1, row=3)

        ttk.Button(frm, text="Fichier Filtre", command=lambda: utils.select_file(filtre)).grid(column=0, row=7, sticky=W)
        ttk.Label(frm, text="LIEN", textvariable=filtre).grid(column=1, row=7)

        self.root2 = parent 
        self.root2.title(title_app) 
        self.root2.protocol("WM_DELETE_WINDOW", lambda: utils.on_closing(self))
        self.root2.resizable(False, False)
    
    def openFrame(self):
        pass

    def call_result(self, pcap, log_atk, log_info, result_json, result_log, filtre):
        if(not os.path.exists(pcap) or not os.path.exists(log_atk) or not os.path.exists(log_info)):
            messagebox.showinfo("Fichier manquant", "Erreur: un ou plusieurs fichiers n'a pas été trouvé")
        else:
            self.root2.withdraw()
            new_window = Toplevel()
            result.open_capture(new_window, pcap, log_atk, log_info, result_json, result_log, filtre)

    def call_calcul(self, pcap, log_atk, log_info, filtre):
        if(not os.path.exists(pcap) or not os.path.exists(log_atk) or not os.path.exists(log_info) or not os.path.exists(filtre)):
            messagebox.showinfo("Fichier manquant", "Erreur: un ou plusieurs fichiers n'a pas été trouvé")
        else:
            self.root2.withdraw()
            new_window = Toplevel()
            calcul.Calcul(new_window, pcap, log_atk, log_info, filtre)