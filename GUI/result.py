from tkinter import * 
from tkinter import ttk
from tkinter import Canvas
from datetime import datetime

import utils
import tkinter
import os
import json

class open_capture(object): 
    def __init__(self, parent, pcap, log_atk, log_info, result_json, result_log, filtre): 
        #Fenêtre d'affichage des captures
        date        = "Date: \t\t"
        seed        = "Seed: \t\t"
        nb_attaque  = "Nombre d'attaque: \t"
        ip          = "IP: \t\t"
        port        = "Port: \t\t"
        duree       = "Temps Capture: \n"
        interface   = "Interface: \t\t"
        attaque     = []

        log_file = open(log_info, 'r')
        Lines = log_file.readlines()
        for line in Lines:
            word = line.split(":")
            if(word[0] == "Date"):
                ####### FORMAT A CONVERTIR: JOUR-MOIS-ANNEE_HEURE-MINUTE-SECONDE
                date += str(datetime.strptime(word[1], '%d-%m-%y_%H-%M-%S'))
            elif(word[0] == "Seed"):
                seed += str(word[1])
            elif(word[0] == "Nb_attaque"):
                nb_attaque += str(word[1])
            elif(word[0] == "IP"):
                ip += str(word[1])
            elif(word[0] == "Port"):
                port += str(word[1])
            elif(word[0] == "Duree"):
                millis = int(word[1])
                seconds=(millis/1000)%60
                seconds = int(seconds)
                minutes=(millis/(1000*60))%60
                minutes = int(minutes)
                hours=(millis/(1000*60*60))%24
                duree += str(" - %d Heures\n - %d Minutes\n - %d Secondes" % (hours, minutes, seconds))
            elif(word[0] == "Interface"):
                interface += str(word[1])
            elif(word[0] == "Attaque"):
                attaque.append(word)

        log_file.close()

        frm = ttk.Frame(parent, padding=10)
        frm.grid()

        ttk.Label(frm, text=date).grid(column=0, row=0, sticky=W)
        ttk.Label(frm, text=seed).grid(column=0, row=1, sticky=W)
        ttk.Label(frm, text=nb_attaque).grid(column=0, row=2, sticky=W)
        ttk.Label(frm, text=ip).grid(column=0, row=3, sticky=W)
        ttk.Label(frm, text=port).grid(column=0, row=4, sticky=W)
        ttk.Label(frm, text=interface).grid(column=0, row=6, sticky=W)

        if(os.path.isfile(result_json)): 
            my_json = open(result_json)
            data = json.load(my_json)
            scenario = data.get("Scenario")
            resultat = data.get("Résultats")

            nom_filtre = scenario[0].get("Filtre")
            ddos_total = scenario[0].get("Packets à filtrer comme DDoS")

            ddos_filtre = resultat[0].get("Packets identifié comme DDoS par le filtre")
            vrai_positif = resultat[0].get("vrai_positif")
            vrai_negatif = resultat[0].get("vrai_negatif")
            faux_positif = resultat[0].get("faux_positif")
            faux_negatif = resultat[0].get("faux_negatif")
            true_positive_rate = resultat[0].get("True Positive Rate")
            true_negative_rate = resultat[0].get("True Negative Rate")
            accuracy = resultat[0].get("Accuracy")

            ttk.Label(frm, text="Filtre: " + str(nom_filtre)).grid(column=0, row=7, sticky=W)
            ttk.Label(frm, text="Nombre de paquets à filtrer: " + str(ddos_total)).grid(column=0, row=8, sticky=W)
            ttk.Label(frm, text="Paquets identifié comme DDoS par le filtre: " + str(ddos_filtre)).grid(column=0, row=9, sticky=W)
            ttk.Label(frm, text="Paquets vrai positif: " + str(vrai_positif)).grid(column=0, row=10, sticky=W)
            ttk.Label(frm, text="Paquets vrai negatif: " + str(vrai_negatif)).grid(column=0, row=11, sticky=W)
            ttk.Label(frm, text="Paquets faux positif: " + str(faux_positif)).grid(column=0, row=12, sticky=W)
            ttk.Label(frm, text="Paquets faux negatif: " + str(faux_negatif)).grid(column=0, row=13, sticky=W)
            ttk.Label(frm, text="True Positive Rate: " + str(round(true_positive_rate*100, 3)) + "%").grid(column=0, row=14, sticky=W)
            ttk.Label(frm, text="True Negative Rate: " + str(round(true_negative_rate*100, 3)) + "%").grid(column=0, row=15, sticky=W)
            ttk.Label(frm, text="Accuracy: " + str(round(accuracy*100, 3)) + "%").grid(column=0, row=16, sticky=W)

            ######## BOUTONS DE FIN ########
            ttk.Label(frm, text="Générer graphes:").grid(column=0, row=21)
            if(os.path.isfile(filtre)):
                ttk.Button(frm, text='Paquets par groupe de 15 secondes', command=lambda: open_capture.genPck(pcap, log_atk, filtre)).grid(column=0, row=22)        
            ttk.Button(frm, text='Camemberts et matrice de confusion', command=lambda: open_capture.genPie(result_json)).grid(column=0, row=23)

        ttk.Label(frm, text=duree).grid(column=0, row=17, rowspan=3, sticky=W)

        canvas = tkinter.Canvas(frm)
        canvas.grid(row=20, column=0)

        vsb = ttk.Scrollbar(frm, orient="vertical", command=canvas.yview)
        vsb.grid(row=20, column=1, sticky='ns')
        canvas.configure(yscrollcommand=vsb.set)

        frame_buttons = tkinter.Frame(canvas)
        canvas.create_window((0, 0), window=frame_buttons)

        i=0
        text_attaque = ""
        for atk in attaque:
            i+=1
            text_attaque += atk[0] + "(" + str(i) + "): " + atk[1] + "\n"
            text_attaque += "\t" + atk[2] + ": " + atk[3] + "\n"
            if(atk[1] != "TCP_Reset_Flood"): text_attaque += "\tDurée: " + str(int(atk[5].split(",")[2][:-1])/1000) + " secondes \n\n"
            else: text_attaque += "\n"

        ttk.Label(frame_buttons, text=text_attaque).grid(column=0, row=0)        
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

        self.root2 = parent 
        self.root2.title("Ouverture Capture") 
        self.root2.protocol("WM_DELETE_WINDOW", lambda: utils.on_closing(self))
        self.root2.resizable(False, False)

    def openFrame(self):
        pass

    def genPck(pcap, log_atk, filtre):
        cmd = "python3 graph.py --pcap " + pcap + " --filtre " + filtre + " --attack " + log_atk + " -ps"
        os.system(cmd)

    def genPie(result_json):
        cmd = "python3 graph.py --result " + result_json + " -p"
        os.system(cmd)
