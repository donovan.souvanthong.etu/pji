from tkinter import * 
from tkinter import ttk
from tkinter import messagebox

import GUI.capture2 as capture2
import utils

class Capture1(object): 
    def __init__(self, parent): 
        # Fenêtre "Création de trace"
        ip = StringVar()
        port = StringVar()
        interface = StringVar()
        temps = StringVar()
        nbAttaque = StringVar()
        nomCapture = StringVar()
        seed = StringVar()
        verbose = IntVar()
        full = IntVar()        
        output = IntVar()
        port.set(0)
        temps.set(0)
        nbAttaque.set(0)

        frm = ttk.Frame(parent, padding=10)
        frm.grid()
        
        ttk.Label(frm, text="Création de capture").grid(column=0, columnspan=6, row=0)


        ttk.Entry(frm, width=15, textvariable=ip).grid(column=0, row=1, sticky=E)
        ttk.Label(frm, text="IP", anchor="e").grid(column=1, row=1, sticky=W)

        ttk.Spinbox(frm, width=5, textvariable=port, increment=1, from_=0, to=65535, justify="right").grid(column=0, row=2, sticky=E)
        ttk.Label(frm, text="Port à attaquer", anchor="e").grid(column=1, row=2, sticky=W)

        ttk.Entry(frm, width=15, textvariable=interface).grid(column=0, row=3, sticky=E)
        ttk.Label(frm, text="interface à écouter", anchor="e").grid(column=1, row=3, sticky=W)

        ttk.Spinbox(frm, width=4, textvariable=temps, increment=1, from_=0, to=3600, justify="right").grid(column=0, row=4, sticky=E)
        ttk.Label(frm, text="Temps de la capture (en secondes)", anchor="e").grid(column=1, row=4, sticky=W)

        ttk.Spinbox(frm, width=2, textvariable=nbAttaque, increment=1, from_=0, to=15, justify="right").grid(column=0, row=5, sticky=E)
        ttk.Label(frm, text="Nombre d'attaque a effecuter", anchor="e").grid(column=1, row=5, sticky=W)

        ttk.Entry(frm, width=20, textvariable=nomCapture).grid(column=0, row=6, sticky=E)
        ttk.Label(frm, text="Nom de la capture", anchor="e").grid(column=1, row=6, sticky=W)

        ttk.Label(frm, text="Arguments optionnels:").grid(column=0, row=7)

        ttk.Entry(frm, width=10, textvariable=seed).grid(column=0, row=8, sticky=E)
        ttk.Label(frm, text="Seed à rejouer", anchor="e").grid(column=1, row=8, sticky=W)

        ttk.Checkbutton(frm, variable=verbose).grid(column=0, row=9, sticky=E)
        ttk.Label(frm, text="Mode Verbose", anchor="e").grid(column=1, row=9, sticky=W)

        ttk.Checkbutton(frm, variable=full).grid(column=0, row=10, sticky=E)
        ttk.Label(frm, text="Mode Full", anchor="e").grid(column=1, row=10, sticky=W)

        ttk.Checkbutton(frm, variable=output).grid(column=0, row=11, sticky=E)
        ttk.Label(frm, text="Output vers terminal", anchor="e").grid(column=1, row=11, sticky=W)

        ttk.Button(frm, text= "Démarrer la capture", command=lambda: Capture1.call_capture2(self, ip.get(), port.get(), interface.get(), temps.get(), nbAttaque.get(), nomCapture.get(), seed.get(), verbose.get(), full.get(), output.get() )).grid(column=4, row=12)

        self.root2 = parent 
        self.root2.title("Capture") 
        self.root2.protocol("WM_DELETE_WINDOW", lambda: utils.on_closing(self))
        self.root2.resizable(False, False)
                                                                
    def openFrame(self):
        pass

    def call_capture2(self, ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, output):
        if(ip == "" or port == "0" or interface == "" or temps == "0" or nomCapture == ""):
            messagebox.showinfo("Arguments manquants", "Erreur: un ou plusieurs arguments sont manquants (ip, port, interface, temps d'attaque ou nom de la capture)")
        else:
            self.root2.withdraw()
            new_window = Toplevel()
            capture2.Capture2(new_window, ip, port, interface, temps, nbAttaque, nomCapture, seed, verbose, full, output)
