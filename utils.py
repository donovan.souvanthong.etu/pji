######################## IMPORT FILTRE POUR LECTURE FICHIER FILTRE ########################
######################## CHANGER D'IMPORT POUR CHAQUE FILTRE  #############################

from ListeFiltres.ddos_detect_reader import * 
# from ListeFiltres.ddos_attack_detection_reader import * 

###########################################################################################
###########################################################################################


import time

def time_in_milli():
    return int(time.time()*1000)

def get_packet_info(packet):  
    src_ip = None
    dst_ip = None
    src_port = None
    dst_port = None

    if("IPV6" in str(packet.layers)):
        src_ip = packet.ipv6.src
        dst_ip = packet.ipv6.dst
    elif("IP" in str(packet.layers)):
        src_ip = packet.ip.src
        dst_ip = packet.ip.dst
    
    if("UDP" in str(packet.layers)):
        src_port = packet.udp.srcport
        dst_port = packet.udp.dstport
    elif("TCP" in str(packet.layers)):
        src_port = packet.tcp.srcport
        dst_port = packet.tcp.dstport

    return src_ip, dst_ip, src_port, dst_port

def read_attack_file(attack):
    attack_paquet = []
    attack_nb_paquet_ddos = 0

    log_file2 = open(attack, 'r')
    Lines = log_file2.readlines()
    for line in Lines:
        word = line.split(";")
        #IP_source, PORT_source, IP_dest, POST_dest
        attack_paquet.append([word[0], word[1], word[2], word[3]])
        attack_nb_paquet_ddos += 1

    log_file2.close()
    
    return attack_paquet, attack_nb_paquet_ddos


def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{p:.1f}%  ({v:d} paquets)'.format(p=pct,v=val)
    return my_autopct











######################## FONCTION GUI ########################
from tkinter import filedialog as fd
from tkinter import messagebox
import sys

def select_folder(folder):
    foldername = fd.askdirectory()
    folder.set(foldername)


def select_file(file):
    filetypes = (
        ('pcap files', '*.pcap'),
        ('result files', '*.json'),
        ('result files', '*.log'),
        ('text files', '*.txt'),
        ('All files', '*.*')
    )

    filename = fd.askopenfilename(
        title='Open a file',
        initialdir='/',
        filetypes=filetypes)

    # showinfo(
    #     title='Selected File',
    #     message=filename
    # )

    file.set(filename)

def on_closing(self):
    if messagebox.askokcancel("Quit", "Do you want to quit ?"):
        self.root2.quit()

def on_closing_std(self, old_stdout):
    if messagebox.askokcancel("Quit", "Do you want to quit ?"):
        sys.stdout = old_stdout
        self.root2.quit()

def on_return(self, parent):
    self.root2.withdraw()
    parent.update()
    parent.deiconify()
    # self.root2.destroy()

class Redirect():
    '''
    Class redirect pour les output de terminal
    '''
    def __init__(self, widget, autoscroll=True):
        self.widget = widget
        self.autoscroll = autoscroll

    def write(self, text):
        self.widget.insert('end', text)
        if self.autoscroll:
            self.widget.see("end")  # autoscroll    
            
    def flush(self):
        pass

