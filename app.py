#!/usr/bin/env python3
from tkinter import * 
from tkinter import ttk
from tkinter import messagebox

import GUI.capture1 as capture1
import GUI.ouvrirAuto as ouvrirAuto

class App(object): 
    # Fenêtre principale de l'application
    def __init__(self, parent): 
        self.root = parent 
        frm = ttk.Frame(parent, padding=10)
        frm.grid()

        ttk.Label(frm, text="Application pour évaluation de défense anti-ddos").grid(column=0, columnspan=5, row=0)

        ttk.Button(frm, text="Creation de trace", command=lambda: App.call_capture1(self)).grid(column=1, row=1, ipadx=5, ipady=5)
        ttk.Button(frm, text="Benchmark", command=lambda: App.call_ouvrir(self, "Benchmark")).grid(column=2, row=1, ipadx=5, ipady=5)
        ttk.Button(frm, text="Ouvrir Capture existante", command=lambda: App.call_ouvrir(self, "trace")).grid(column=3, row=1, ipadx=5, ipady=5)
        ttk.Button(frm, text="Quit", command=root.quit).grid(column=4, row=1, ipadx=5, ipady=5)

    def call_capture1(self):
        self.root.withdraw()
        new_window = Toplevel()
        capture1.Capture1(new_window)

    def call_ouvrir(self, app):
        self.root.withdraw()
        new_window = Toplevel()
        ouvrirAuto.ouvrirAuto(new_window, app)
        
def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()

root = Tk()
app = App(root)
root.protocol("WM_DELETE_WINDOW", on_closing)
root.title("GUI Benchmark DDoS")
root.resizable(False, False)
root.mainloop()
root.quit()