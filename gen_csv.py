#!/usr/bin/env python3

import csv
import json
import os

print("Génération compare_result.csv")
csvfile = open('compare_result.csv', 'w', newline='')
writer_csv = csv.writer(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
writer_csv.writerow(['Capture'] + ['NomFichier'] + ['True Positive Rate'] + ['True Negative Rate'] + ['Accuracy'])

captures = os.listdir("Captures")
print("Dossier captures:", captures)

for file in captures:
    if os.path.isdir("Captures/"+file):
        print("\n-----------------------------")
        print("- Recherche result in " + file)
        result = os.listdir("Captures/"+file)
        
        for file2 in result:
            if(".json" in file2):
                print(file2)
                my_json = open("Captures/" + file + "/" + file2)
                data = json.load(my_json)

                scenario = data.get("Scenario")
                resultat = data.get("Résultats")

                nom_filtre = scenario[0].get("Filtre")
                tpr = resultat[0].get("True Positive Rate")
                tnr = resultat[0].get("True Negative Rate")
                accu = resultat[0].get("Accuracy")

                writer_csv.writerow([str(file)] + [str(nom_filtre)] + [str(tpr).replace(".",",")] + [str(tnr).replace(".",",")] + [str(accu).replace(".",",")])
