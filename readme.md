# Benchmark pour IDs anti DDoS

- M1 PJI 2020-2021
- M2 PFE 2021-2022
- Sujet: Benchmark pour IDs anti DDoS
- Encadrant: Gilles Grimaud
- Etudiant: Donovan Souvanthong

# Sommaire
1. [Prérequis](#Prérequis)
    - [Librairies](#Librairies)
    - [Outils (Linux)](#Outils-(Linux))
    - [Outils (MacOS)](#Outils-(MacOS))
2. [Résumé du projet](#Résumé-du-projet)
3. [Problématique](#Problématique)
4. [Présentation du répot](#Présentation-du-répot)
    - [Executable](#Executable)
        - [Update PFE](#Update-PFE)
    - [Dossiers](#Dossiers)
    - [Ressources](#Ressources)
5. [GUI](#GUI)
6. [Modification](#Modification)
    - [Ajouter des filtres](#Ajouter-des-filtres)
    - [Ajouter des attaques](#Ajouter-des-attaques)
7. [Analyse](#Analyse)
    - [Resultats](#Resultats)
        - [True Positive Rate](#True-Positive-Rate)
        - [True Negative Rate](#True-Negative-Rate)
        - [Accuracy](#Accuracy)
        - [Conclusion](#Accuracy)
8. [Seed exemple](#Seed-exemple)
9. [References/Bibliographie](#References/Bibliographie)
    - [Bibliographie](#Bibliographie)
    - [Filtres](#Filtres)
    - [Script d'attaques](#Script-d'attaques)

# Prérequis

Les prérequis pour pouvoir exécuter le projet sont les suivants:

- pyshark
- scapy
- tshark
- pcapfix

## Librairies:

````
sudo pip3 install pyshark
sudo pip3 install scapy
````

## Outils (Linux):

```
sudo apt install tshark
sudo apt install pcapfix
```

## Outils (MacOS):
L'installation de tshark se fait avec la commande suivante:
```
brew install --cask wireshark
```

De plus pcapfix ne peut être installé avec Homebrew, à la place il faut pull le repot git:
```
git pull https://github.com/Rup0rt/pcapfix
```


# Résumé du projet
Les attaques en dénie de service de l'internet et du cloud sont devenues monnaie courante. Un certain nombre d'algorithmes sont proposés par l'état de l'art. Néanmoins la communauté scientifique ne dispose pas d'outil de benchmark pour évaluer ces systèmes de détection de DDoS. Dans ce PJI nous nous proposons de traiter ce sujet. 

# Problématique
L'objet de ce projet individuel est de proposer un ensemble de benchmark permettant d'évaluer différentes stratégies de détection des attaques en dénie de service du cloud. 

# Présentation du répot 

## Executable

- [main.py](main.py)

Fichier principal, permet d'exécuter de manière simultanée une capture de trame ([trace.py](trace.py)), et d'attaquer une adresse IP à intervalle aléatoire ([attack.py](attack.py)) 

Prend en paramètres:

- `-ip` (ip à attaquer)
- `-port` (port à attaquer)
- `--time` (Temps en seconde de la capture)
- `--nb_atk` (Nombre d'attaque à effectuer pendant la capture)
- `--interface` (Interface à écouter ajouter espace si plusieurs interfaces)
- `--output` (Nom du fichier de la capture)
- `--seed` (seed à rejouer. Permet de rejouer un scénario précis d'attaque. Voir section [seed exemple](#seed-exemple))
- `--verbose` (Mode verbose)
- `--full` (Capture avec chaque attaque tirée entre 1 et 3 fois par défaut. L'argument "--nb_atk" n'a aucun impact si "--full" est activé)

````
Exemple:
sudo main.py -ip ADRESSE_IP -port PORT --time TEMPS_SECONDES --nb_atk NB_ATTAQUE --interface INTERFACE1 INTERFACE2 --output NOM_FICHIER.pcap --seed NUMERO_SEED --verbose

sudo main.py -ip ADRESSE_IP -port PORT --time TEMPS_SECONDES --full --interface INTERFACE1 INTERFACE2 --output NOM_FICHIER.pcap --seed NUMERO_SEED --verbose
````

- [bench.py](bench.py)

**Attention** à vérifier les imports dans le fichier ~~[bench.py](bench.py)~~ [utils.py](utils.py) à chaque utilisation pour des résultats cohérents.

Fichier de benchmark premettant d'analyser les résultats de filtrage sur une capture donnée.

Prend en paramètres:

- `--log` (Fichier log de la capture)
- `--attack` (Fichier attaque de la capture)
- `--filtre` (Fichier résultant du filtre mis en place)
- `--pcap` (Fichier pcap de la capture)
- `--verbose` (Mode verbose)

```
Exemple dans Capture0:
python3 ../../bench.py --log 1620502026_08-05-21_19-27-06_test.pcap.log --filtre DDoS-Detect_TRICHE.txt --pcap capture_fixed.pcap --attack list_packet_attack_capture.pcap.log --verbose
```

- [gen_result.sh](gen_result.sh)

**Attention** à vérifier les imports dans le fichier ~~[bench.py](bench.py)~~ [utils.py](utils.py) à chaque utilisation pour des résultats cohérents.

Permet d'analyser avec le benchmark toutes les captures présentes et de générer tous les fichiers résultats (.json)

Prend en paramètres:

- 1 (filtre: [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect))
- 2 (filtre: [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System))

```
Exemple:
./gen_result.sh 1
```

- [gen_csv.py](gen_csv.py)

Permet de générer un fichier csv de tous les fichiers résultats (.json) présents dans tous les dossiers des captures. Cela nous permet de comparer les différents résultats de chaque analyse.

Ne prends aucun paramètre.

```
./gen_csv.py
```

### Update PFE:

- [app.py](app.py)

Permet de lancer l'interface graphique voir [GUI](GUI) 

```
Dans le dossier principal:
python3 app.py
```

- [graph.py](graph.py)

**Attention** à vérifier les imports dans le fichier ~~[bench.py](bench.py)~~ [utils.py](utils.py) à chaque utilisation pour des résultats cohérents.

Fichier permettant de générer deux fichier svg (image vectorielle):
1. Une image "paquets_secondes.svg" qui contient:   
    - Les courbes de nombres de paquets légitimes/DDoS/total
    - Les zones contenants des paquets DDoS émis/détectés/émis et detectés
    - Les calculs de sensibilités et spécificités 

Elle prend en paramètres `--filtre`, `--pcap`, `--attack` et `-ps`

```
Exemple dans Capture0:
python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture_fixed.pcap --attack list_packet_attack_capture.pcap.log -ps
```


2. Une image contenant:
    - La matrice de confusion
    - Un camembert de répartition des paquets réseau
    - Un camembert de répartition des paquets réseau parmi les paquets légitimes
    - Un camembert de répartition des paquets réseau parmi les paquets attaquant

Elle prends en paramètres `--result` et `-p`

```
Exemple dans Capture0
python3 ../../graph.py --result result_ddos_detect.json -p
```


Le script prend les paramètres suivants:

- `--attack` (Fichier attaque de la capture)
- `--filtre` (Fichier résultant du filtre mis en place)
- `--pcap` (Fichier pcap de la capture)
- `--result` (Fichier résultat)
- `--packet_seconds` ou `-ps` (Création du graphe paquets/minutes)
- `--pie` ou `-p` (Création des camemberts avec la matrice de confusion)

Note: les deux graphiques peuvent être générés en même temps

- [pcapMark.py](pcapMark.py)

**Attention** à vérifier les imports dans le fichier ~~[bench.py](bench.py)~~ [utils.py](utils.py) à chaque utilisation pour des résultats cohérents.

Permet de commenter toute la capture envoyée en marquant les paquets "émis DDoS", "detecté DDoS" et "émis et detecté DDoS"

Une fois marqués des filtres wireshark sont disponibles n'afficher que certaines informations:
- Émis DDoS: `frame.comment == "Emitted DDoS "`
    - Note: l'espace après `DDoS` doit obligatoirement être mis pour obtenir des résultats
- Détecté DDoS: `frame.comment == "Detected DDoS"`
- Émis DDoS et détecté DDoS: `frame.comment == "Emitted DDoS Detected DDoS"`

Plusieurs filtres peuvent être cumulés dans wireshark

- Exemple:
    - "Émis DDoS non detecté" et "émis ddos et detecté": `frame.comment == "Emitted DDoS " or frame.comment == "Emitted DDoS Detected DDoS"` 

Le script prends les paramètres suivants:

```
Exemple dans Capture0:
python3 ../../pcapMark.py --filtre DDoS-Detect_TRICHE.txt --pcap capture_fixed.pcap --attack list_packet_attack_capture.pcap.log 
```

- [gen_graph.sh](gen_graph.sh)

**Attention** à vérifier les imports dans le fichier ~~[bench.py](bench.py)~~ [utils.py](utils.py) à chaque utilisation pour des résultats cohérents.

Permet de générer avec le script [graph.py](graph.py) tous les graphiques de toutes les captures présentes

Prend en paramètres:

- 1 (filtre: [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect))
- 2 (filtre: [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System))

## Dossiers

- [Captures](Captures)

Ce dossier contient toutes les captures effectuées à l'aide des scripts présent dans le repot.
Il y a 10 captures et les captures sont les suivantes:

- Capture0 à Capture5: captures en mode "**full**" (Capture avec chaque attaque tirée entre 1 et 3 fois par défaut). 

- Capture6: Seulement attaque type "**UDP_Flood**"

- Capture7: Seulement attaque type "**SYN_Flood**"

- Capture8: Seulement attaque type "**TCP_Ack_Flood**"

- Capture9: Seulement attaque type "**TCP_Reset_Flood**" (Attaque importée dans le cadre du PJI mais retiré pendant le PFE)

Toutes les captures ont une durée d'une heure et un dossier de captures contient les fichiers suivants:

1. Un fichier log du scénario de la capture
2. La capture de trame
3. L'analyse de la trame par les filtres (.txt)
4. La liste des paquets attaquants (IP_src;Port_src;IP_dst:Port_dst)
5. Les résultats du benchmark avec les différents filtres (.json)
6. Les fichiers contenant les numéros des paquets malveillants par les différents filtres

**Remarque**: les fichiers "`DDoS-Detect_TRICHE.txt`" sont les fichiers utilisés par le benchmark. Il s'agit tout simplement des meme fichiers que "`DDoS-Detect.txt`" à l'exception que les paquets DDoS et les paquets légitimes sont inversés.

- [ListeAttaques](ListeAttaques)

Ce dossier contient toutes les attaques effectuées lors des captures.
Il contient 4 attaques:

1. [SYN_Flood](https://github.com/PhynX404/ddos-tools/blob/main/SYN-Flood.py)
2. [TCP_Ack_Flood](https://github.com/uiucseclab/460FinalDDoSAttacks/blob/master/TCP%20PSH%2BACK%20Flood/tcp_psh_ack.py)
3. ~~[TCP_Reset_Flood](https://gist.github.com/spinpx/263a2ed86f974a55d35cf6c3a2541dc2)~~ (Attaque présente dans le dossier mais pas implémenté)
4. [UDP_Flood](https://github.com/PhynX404/ddos-tools/blob/main/flood_udp.py)

D'autres attaques peuvent être ajoutés (Voir section [Modification](#Modification)).

- [ListeFiltres](ListeFiltres)

Deux filtres ont été utilisés pour tester le benchmark:

- [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect)
- [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System)

D'autres filtres peuvent être ajoutés (Voir section [Modification](#Modification)).

- [GUI](GUI)

Ce dossier contient tous les fichiers python pour l'affichage des différentes fenêtres de l'interface graphique

Il contient aussi un fichier de configuration (`default.conf`) avec les noms des fichiers à aller chercher de manière automatique (Voir section [Modification](#Modification)).

## [Ressources](Ressources)

Dossier contenant les images de la partie analyse ainsi que des tableaux de comparaisons de résultats.

# GUI

L'application se présente de cette manière à l'ouverture:

![mainApp.png](Ressources/GUI/mainApp.png "Main")

Il y a 4 boutons:
- `Création de trace` 
- `Benchmark`
- `Ouvrir Capture existante`
- `Quit`


## Création de trace
Après avoir cliqué sur "`Création de trace`" cette fenêtre s'ouvre
![capture1.png](Ressources/GUI/capture1.png "Capture")

Elle permet de remplir tous les paramètres nécessaire à la capture d'une trame. Le script en ligne de commande est [main.py](main.py) (voir section [Executable](#Executable) pour les différents arguments)

- Exemple
![capture2.png](Ressources/GUI/capture2.png "Capture")

Si jamais il manque un ou plusieurs arguments, un message d'erreur sera affiché

![erreur3.png](Ressources/GUI/erreur3.png "Erreur")

Une fois que l'on clique sur "`Démarrer la capture`" cette fenêtre s'ouvre, récapitulant les informations une dernière fois.
Il faut ensuite cliquer sur "`Début de la Capture`" pour démarrer la capture.
La sortie de la commande sera affiché dans l'encadré blanc.
![capture3.png](Ressources/GUI/capture3.png "Capture")

## Benchmark 
Après avoir cliqué sur "`Benchmark`" cette fenêtre s'ouvre. Le script en ligne de commande est [bench.py](bench.py) (voir section [Executable](#Executable) pour les différents arguments)

![bench1.png](Ressources/GUI/bench1.png "Bench")
Il faut alors cliquer sur "`Lien Dossier`" pour aller chercher le dossier qui contient tous les fichiers nécessaires au Benchmark
![dossier.png](Ressources/GUI/dossier.png "Dossier")

![bench2.png](Ressources/GUI/bench2.png "Bench")

Le bouton "`Saisie manuel`" permet de vérifier que les fichiers ont bien été trouvé et de sélectionné. 
![bench6.png](Ressources/GUI/bench6.png "Bench")

Si jamais un fichier est manquant un message d'erreur sera affiché lorsque l'on clique sur "`Valider Benchmark`"
![erreur2.png](Ressources/GUI/erreur2.png "Erreur")

Une fois que l'on clique sur "`Valider Benchmark`" cette fenêtre s'ouvre.
Il faut ensuite cliquer sur "`Début du calcul`" pour démarrer le benchmark.
La sortie de la commande sera affiché dans l'encadré blanc.
![bench5.png](Ressources/GUI/bench5.png "Bench")

## Ouvrir Capture existante
Après avoir cliqué sur "`Création de trace`" cette fenêtre s'ouvre
![trace1.png](Ressources/GUI/trace1.png "Trace")

Il faut alors cliquer sur "`Lien Dossier`" pour aller chercher le dossier qui contient tous les fichiers nécessaires au Benchmark. Le bouton "`Saisie manuel`" permet de vérifier que les fichiers ont bien été trouvé et de sélectionné. Si jamais un fichier est manquant un message d'erreur sera affiché lorsque l'on clique sur "`Valider Benchmark`"
![trace4.png](Ressources/GUI/trace4.png "Trace")

Deux affichages sont possibles si l'on précise le fichier "`Résultat json`"

### Avec fichier résultat json:

Note: le bouton "`Paquets par groupe de 15 secondes`" sera masqué si le fichier `Filtre` n'est pas précisé
![trace5.png](Ressources/GUI/trace5.png "Trace")

### Sans fichier résultat json:
![trace7.png](Ressources/GUI/trace7.png "Trace")


# Modification 

## Ajouter des filtres

1. Créer un nouveau fichier pour le filtre dans le dossier [ListeAttaques](ListeAttaques)
2. Réimplémenter [ids_reader.py](ListeFiltres/ids_reader.py) dans le nouveau fichier avec les méthodes `read()` et `get_filter_name()` (Voir: [ddos_detect_reader.py](ListeFiltres/ddos_detect_reader.py) et [ddos_attack_detection_reader.py](ListeFiltres/ddos_attack_detection_reader.py))
3. **Absolument changer** les imports dans ~~[bench.py](bench.py)~~ [utils.py](utils.py) pour qu'il corresponde au nouveau fichier


## Ajouter des attaques

1. Ajouter le fichier d'attaque dans le dossier [ListeAttaques](ListeAttaques)
2. Importer la nouvelle attaque et modifier toutes les méthodes dans [Attaques.py](ListeAttaques/Attaques.py) pour qu'elle puisse avoir une chance d'etre effectuée.

## GUI: Modifier la sélection des fichiers automatique dans la fenêtre "Benchmark" et "Ouverture de trace" (ouvrirAuto.py)

1. Modifier la section `[CUSTOM]` dans le fichier [default.conf](GUI/default.conf) (dossier [GUI](GUI))

### OU

1. Ajouter une section dans le fichier [default.conf](GUI/default.conf) (dossier [GUI](GUI), voir les autres sections pour l'exemple)

2. Modifier dans le fichier [ouvrirAuto.py](GUI/ouvrirAuto.py) la variable `section` **ligne 62** pour qu'elle corresponde à la section rajoutée

3. Modifier dans le fichier [ouvrirManuel.py](GUI/ouvrirManuel.py) la variable `section` **ligne 28** pour qu'elle corresponde à la section rajoutée


# Analyse

La création du [benchmark](bench.py) est basée sur l'article suivant: "[Defense Mechanisms Against DDoS Attacks in a Cloud Computing Environment: State-of-the-Art and Research Challenges](https://ieeexplore-ieee-org.ressources-electroniques.univ-lille.fr/document/8794618)" (VI. PERFORMANCE MEASUREMENT METRICS).

Avec les calculs de:

- TPR (True Positive Rate):

![TPR_calcul.png](Ressources/TPR_calcul.png "True Positive Rate")

- TNR (True Negative Rate):

![TNR_calcul.png](Ressources/TNR_calcul.png "True Negative Rate")

- Accuracy (Précision):

![Acc_calcul.png](Ressources/Acc_calcul.png "Accuracy")

Le script de [benchmark](bench.py) va lire la capture, la liste des paquets attaquant, ainsi que le fichier donné par le filtre et va calculer le TPR, TNR, et l'Accuracy.

## Resultats

Les résultats varie entre 0 et 1, le plus proche de 1 étant le meilleur.

Après avoir effectué le benchmark sur toutes les captures, la comparaison des résultats est disponible dans le fichier ([Ressources/compare_result.ods](Ressources/compare_result.ods)). 

Les captures sont les suivantes:

- Capture0 à Capture5: captures en mode "**full**" (Capture avec chaque attaque tirée entre 1 et 3 fois par défaut). 

- Capture6: Seulement attaque type "**UDP_Flood**"

- Capture7: Seulement attaque type "**SYN_Flood**"

- Capture8: Seulement attaque type "**TCP_Ack_Flood**"

- Capture9: Seulement attaque type "**TCP_Reset_Flood**" (Attaque importée dans le cadre du PJI mais retiré pendant le PFE)

Toutes les captures ont une durée d'une heure.

### True Positive Rate
![TPR.png](Ressources/TPR.png "True Positive Rate")

En terme de détection de paquet malveillant, le filtre [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System) semble etre meilleur que [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect). Il arrive parfois a retrouver l'entièreté des paquets malveillants de certaines captures. 

Sur les captures [1](Captures/Capture1), [6](Captures/Capture6), [7](Captures/Capture7), et [8](Captures/Capture8), le score de TPR obtenu est de 1 pour [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System)
contre 0.93, 0.96, 0.87, 0.93 pour [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect). Et le score TPR est globalement meilleur sur les autres captures 

### True Negative Rate
![TNR.png](Ressources/TNR.png "True Negative Rate")

Cependant en terme de détection de paquet légitime, [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect) semble obtenir de meilleur score sur toutes les captures, à l'exception de la [Capture5](Captures/Capture5):
    
- [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect): 0,827
- [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System): 0,831

La plus grande différence de score étant sur la [Capture8](Captures/Capture8):
    
- [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect): 0,961
- [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System): 0,875


### Accuracy
![Accu.png](Ressources/Acc.png "Accuracy")

Malgré des scores de TPR inférieurs le filtre [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect) semble avoir une meilleure précision. En effet sur les 10 captures effectuées, [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect) obtient un meilleur score sur 6 d'entre elles (Capture [1](Captures/Capture1), [2](Captures/Capture2), [3](Captures/Capture3), [4](Captures/Capture4), [7](Captures/Capture7), et [9](Captures/Capture9)).

Mais le plus gros écart de précision est obtenu par le filtre [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System) pour la [Capture8](Captures/Capture8):

- [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect): 0,937
- [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System): 0,978

### Conclusion

En comparant les résultats des différents scores obtenu par les deux filtres. Nous pouvons en conclure les choses suivantes:

Dans la majorité des captures, le filtre [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System) semble marquer beaucoup plus de paquets ddos que [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect), ce qui lui permet d'identifier dans certains cas tous les paquets malveillants. 

Cependant, c'est aussi ce qui lui fait défaut pour identifier correctement du traffic légitime, et cela impact ses scores, TNR et Accuracy (ou précision) en conséquence.

En identifiant moins de paquet, le filtre [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect) semble etre plus précis dans la plus part des cas.


# Seed exemple

Pour tester une attaque de chaque type: 

- ~~TCP-RESET: 1617193449~~ (Attaque retirée) 
- TCP-ACK:   1617191199
- UDP_FLOOD: 1617190803
- SYN_FLOOD: 1617190376

Commande:
```
sudo main.py -ip ADRESSE_IP -port PORT --time TEMPS_SECONDES --nb_atk 1 --interface INTERFACE_CAPTURE --output NOM_FICHIER.pcap --seed NUMERO_SEED 
```

# References/Bibliographie

## Bibliographie
- [Defense Mechanisms Against DDoS Attacks in a Cloud Computing Environment: State-of-the-Art and Research Challenges](https://ieeexplore-ieee-org.ressources-electroniques.univ-lille.fr/document/8794618)
- [Benchmarks for DDOS Defense Evaluation](https://ieeexplore-ieee-org.ressources-electroniques.univ-lille.fr/document/4086729)
- [How to Test DoS Defenses](https://ieeexplore-ieee-org.ressources-electroniques.univ-lille.fr/document/4804432) 

## Filtres
- [DDoS-Detect](https://github.com/thilak-reddy/DDoS-Detect)
- [DDoS-Attack-Detection-System](https://github.com/hashirkk07/DDoS-Attack-Detection-System)

## Script d'attaques
- [SYN_Flood](https://github.com/PhynX404/ddos-tools/blob/main/SYN-Flood.py)
- [TCP_Ack_Flood](https://github.com/uiucseclab/460FinalDDoSAttacks/blob/master/TCP%20PSH%2BACK%20Flood/tcp_psh_ack.py)
- [TCP_Reset_Flood](https://gist.github.com/spinpx/263a2ed86f974a55d35cf6c3a2541dc2)
- [UDP_Flood](https://github.com/PhynX404/ddos-tools/blob/main/flood_udp.py)