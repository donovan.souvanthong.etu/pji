#!/usr/bin/env python3

import time
import random
import utils
from struct import *
from scapy.all import *
from argparse import ArgumentParser
from ListeAttaques.Attaques import *

log_file = None
ip_source = None
port_source = None
ip_dst = None
port_dst = None


def log(nb_attaque, ip, port, duration, interface, liste_attaques, seed, output_file):
    """
    Fonction qui permet de créer le fichier de log
    """

    t = datetime.now().strftime('%d-%m-%y_%H-%M-%S')
    path = './' + str(seed) + '_' + str(t) + '_' + output_file + '.log'
    log_file = open(path,'w')

    log_file.write("FORMAT:\"SEED_JOUR-MOIS-ANNEE_HEURE-MINUTE-SECONDE.log\": \n")
    log_file.write("Date:" + str(t) + ": \n")
    log_file.write("Seed:" + str(seed) + ": \n")
    log_file.write("Nb_attaque:" + str(nb_attaque) + ": \n")
    log_file.write("IP:" + ip + ": \n")
    log_file.write("Port:" + str(port) + ": \n")
    log_file.write("Duree:" + str(duration) + ":  \n")
    log_file.write("Interface:" + str(interface) + ": \n")

    for atk in liste_attaques:
        #log_file.write(str(atk) + "\n")
        log_file.write("Attaque:" + str(atk[1].__name__) + ":")
        log_file.write("Debut:" + str(atk[0]/1000) + ":")
        log_file.write("Arguments:" + str(atk[2]) + ": \n")

    log_file.close()
    return True

def random_attack(nb_attaque, ip, port, duration, interface, verbose, seed, output_file, full_attack):
    """
    Fonction principale de attack.py
    """ 

    t = datetime.now().strftime('%d-%m-%y_%H-%M-%S')
    path = './list_packet_attack_' + output_file + '.log'
    global log_file 
    log_file = open(path,'w')

    global ip_source
    global port_source
    global ip_dst 
    ip_dst = ip

    global port_dst 
    port_dst = port
    
    attaque_a_lancer = []

    if(full_attack):
        attack = list_attack_full()
            
        column = 1
        nb_attaque = sum(row[column] for row in attack)
        interval = duration/nb_attaque
        interval_cumul = 0
        duree_max = duration/60

        print("Liste_attaque: ", attack)
        print("Nb_attaque: ", nb_attaque)
        print("Duree_max: ", duree_max)
        print("Interval: ", interval)

        for x in range (0, nb_attaque):
            #départ l'attaque
            depart = random.randint(utils.time_in_milli()+int(interval_cumul), (utils.time_in_milli()+int(interval_cumul+interval)))                 
            interval_cumul += interval

            #prends une attaque au hasard 
            atk = random.choice(attack)                                                            
            atk[1]-=1
            if(atk[1] == 0):
                attack.remove(atk)
            print("atk: ", atk)
            print("interval_cumul: ", interval_cumul)
           
            args = args_attack_full(atk, ip, port, interface, duree_max)
            attaque_a_lancer.append([depart, atk[0], args])

    else:
        attack = list_attack()

        for x in range (0, nb_attaque):
            #départ l'attaque
            depart = random.randint(utils.time_in_milli(), (utils.time_in_milli() + duration))  
            #prends une attaque au hasard               
            atk = random.choice(attack)                                                            

            args = args_attack(atk, ip, port, interface, duration)
            attaque_a_lancer.append([depart, atk, args])

        
    log(nb_attaque, ip, port, duration, interface, attaque_a_lancer, seed, output_file)
    
    print("Attaques prévues:")
    print("[Début de l'attaque, Type d'attaque, arguments]")
    print(*attaque_a_lancer, sep='\n')

    timeout = utils.time_in_milli() + duration
    while utils.time_in_milli() < timeout:
        time.sleep(1)
        if(verbose):
            print("Temps restant: ", int(timeout-utils.time_in_milli()), "ms" )
        
        for x in attaque_a_lancer:
            if(utils.time_in_milli() >= x[0]):
                t = threading.Thread( target=x[1], args=x[2]    )
                t.start()
                attaque_a_lancer.remove(x)

    
    log_file.close()
    print("En attente de la fin des thread")
    print("fin")
    return True


def main():
    """ 
    Fonction permettant de lancer le script attack 
    """

    parser = ArgumentParser()
    parser.add_argument('-ip', help='Ip à attaquer')
    parser.add_argument('-port', '-p', help='Port à attaquer')
    parser.add_argument('--time', '-t', help='Temps en seconde de la capture')
    parser.add_argument('--nb_atk', '-nb', help='Nombre d\'attaque à effectuer pendant la capture')
    parser.add_argument('--interface', '-i', nargs='*', help='Interface à écouter (ajouter espace si plusieurs interfaces ex: \"--interface wlp2s0 lo\")')
    parser.add_argument('--output', '-o', help='Nom du fichier de la capture')
    parser.add_argument('--seed', '-s', help='seed a rejouer (permet de rejouer un scénario précis d\'attaque)')
    parser.add_argument("--verbose", '-v', action='store_true', help="Mode verbose")
    parser.add_argument("--full", '-f', action='store_true', help="Capture avec chaque attaque tirée entre 1 et 3 fois (L'argument \"--nb_atk\" n'a aucun impact si \"--full\" est activé)")
    
    args = parser.parse_args()

    if args.ip is not None:
        print("---------------------------------------")
        if args.seed is not None:
            seed = args.seed
            random.seed(int(args.seed))
            print("SEED REJOUE:", seed)
        else:
            seed = int(time.time())
            random.seed(seed)
            print("SEED ALEATOIRE:", seed)
        print("---------------------------------------\n")


        verbose=False
        if args.verbose:
            print("---------------------------------------")
            print("Verbose activé")
            verbose=True
            print("---------------------------------------\n")

        if args.nb_atk is None:
            nb_atk = 0
        else:
            nb_atk = int(args.nb_atk)

        random_attack(nb_atk, args.ip, int(args.port), int(args.time)*1000, args.interface, verbose, seed, args.output, args.full)
    else:
        parser.print_help()



if __name__ == '__main__':
    main()