from .ids_reader import *

class ids_reader(ids_reader):

    def __init__(self, filtre):
        self.filtre = filtre
    
    def read(self):
        filtre_nb_paquet_ddos = 0
        filtre_paquet_ddos = []
        add_paquet = False

        filtre_file = open(self.filtre, 'r')
        Lines = filtre_file.readlines()
        for line in Lines:
            words = line.split(" ")
            #Normalement cela ne devrait pas etre "benign" mais "melicious"
            #Le filtre semblait inverser les paquets légitimes avec les paquets marqué "ddos"
            if("benign" in line):
                add_paquet = True
                continue

            if(add_paquet and words[0].isnumeric()):
                filtre_paquet_ddos.append(int(words[0]))
                filtre_nb_paquet_ddos += 1

        filtre_file.close()

        return filtre_nb_paquet_ddos, filtre_paquet_ddos

    def get_filter_name(self):
        return "ddos_attack_detection"