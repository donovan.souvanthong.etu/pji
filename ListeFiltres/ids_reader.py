from abc import ABCMeta, abstractmethod

class ids_reader(object, metaclass=ABCMeta):
    """ 
    Classe à ré-implémenter selon les filtres avec:
        "read()" pour la lecture du fichier filtre
                qui doit absolument renvoyer deux choses:
                    -Le nombre de paquets marqué "ddos"
                    -Une liste comprenant le numéro des paquets marqué "ddos"

        "get_filter_name()" pour avoir le nom du filtre
    """

    @abstractmethod
    def read(self):
        pass

    @abstractmethod
    def get_filter_name(self):
        pass
