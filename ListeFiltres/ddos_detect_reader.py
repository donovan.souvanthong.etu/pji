from .ids_reader import *

class ids_reader(ids_reader):

    def __init__(self, filtre):
        self.filtre = filtre

    def read(self):
        filtre_nb_paquet_ddos = 0
        filtre_paquet_ddos = []

        filtre_file = open(self.filtre, 'r')
        Lines = filtre_file.readlines()
        for line in Lines:
            words = line.split("|")
            if(words[3] == "ddos_attack\n"):
                filtre_paquet_ddos.append(int(words[0]))
                filtre_nb_paquet_ddos += 1

        filtre_file.close()

        return filtre_nb_paquet_ddos, filtre_paquet_ddos

    def get_filter_name(self):
        return "ddos_detect"
    