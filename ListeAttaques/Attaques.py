import random
from .SYN_Flood import SYN_Flood
from .UDP_Flood import UDP_Flood
# from .TCP_Reset_Flood import TCP_Reset_Flood
from .TCP_Ack_Flood import TCP_Ack_Flood

def list_attack():
    """
    Permet de récupérer la liste des fonctions pour les attaques aléatoires
    """
    return [UDP_Flood,
            SYN_Flood,
            # TCP_Reset_Flood,
            TCP_Ack_Flood]

def list_attack_full():
    """
    Permet de récupérer la liste des fonctions pour les attaques aléatoires en mode "Full" c'est a dire 
    Chaque attaque effectuée par un intervalle tiré de manière aléatoire.

    Par défaut: 1 à 3 
    """
    return [[UDP_Flood, random.randint(1, 3)],
            [SYN_Flood, random.randint(1, 3)],
            # [TCP_Reset_Flood, random.randint(1, 3)],
            [TCP_Ack_Flood, random.randint(1, 3)]]

def args_attack_full(atk, ip, port, interface, duree_max):
    """
    Permet de gérer les arguments de chaque attaque
    """
    if(atk[0] == UDP_Flood):
        #UDP_Flood #durée de l'attaque qui varie 
        args = (ip, port, random.randint(0, duree_max))                

    elif (atk[0] == SYN_Flood):
        #SYN_Flood #durée de l'attaque qui varie
        args = (ip, port, random.randint(0, duree_max))                

    # elif (atk[0] == TCP_Reset_Flood):
    #     #TCP_Reset_Flood 
    #     args = (ip, interface, random.randint(0, duree_max))             

    elif (atk[0] == TCP_Ack_Flood):
        #TCP_Ack_Flood #durée de l'attaque qui varie
        args = (ip, port, random.randint(0, duree_max))    
          
    return args

def args_attack(atk, ip, port, interface, duration):
    """
    Permet de gérer les arguments de chaque attaque
    """
    if(atk == UDP_Flood): 
        #UDP_Flood #durée de l'attaque qui varie
        args = (ip, port, random.randint(int((1*duration)/100), int((10*duration)/100)))               

    elif (atk == SYN_Flood):
        #SYN_Flood #durée de l'attaque qui varie
        args = (ip, port, random.randint(int((1*duration)/100), int((10*duration)/100)))                

    # elif (atk == TCP_Reset_Flood):
    #     #TCP_Reset_Flood 
    #     args = (ip, interface, random.randint(int((1*duration)/100), int((10*duration)/100)))             

    elif (atk == TCP_Ack_Flood):
        #TCP_Ack_Flood #durée de l'attaque qui varie
        args = (ip, port, random.randint(int((1*duration)/100), int((10*duration)/100)))  
    
    return args 