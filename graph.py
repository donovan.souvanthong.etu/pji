#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import numpy as np
import pyshark
import json
import utils
from argparse import ArgumentParser

def paquets_minutes(filtre, pcap, attack):
    """ 
    Fonction permettant de créer le graph paquets par secondes 
    """
    ##### Variable pour fichier attack
    attack_paquet = []
    attack_nb_paquet_ddos = 0

    ##### Variable pour fichier filtre
    filtre_nb_paquet_ddos = 0
    filtre_paquet_ddos = []

    ######################################### LECTURE FICHIER ATTACK ######################################### 
    attack_paquet, attack_nb_paquet_ddos = utils.read_attack_file(attack)

    ######################################### LECTURE FICHIER FILTRE #########################################
    reader = utils.ids_reader(filtre)
    filtre_nb_paquet_ddos, filtre_paquet_ddos = reader.read()

    ######################################### LECTURE FICHIER PCAP #########################################
    nb_paquet = []
    nb_paquet_ddos = []
    nb_paquet_legit = []

    shark_cap = pyshark.FileCapture(pcap)

    #################### variable pour les courbes paquets/ddos/legitime
    seconde_exact = 0
    seconde_temp = 0
    num_paquet_total = 0
    paquet = 0
    paquet_ddos = 0
    paquet_legit = 0
    fst_timestamp = 0

    #################### variable sensi/speci
    vrai_positif = 0
    vrai_negatif = 0
    faux_positif = 0
    faux_negatif = 0
    sensi_temp = []
    speci_temp = []

    # sensi_max_display = []
    # sensi_min_display = []
    # speci_max_display = []
    # speci_min_display = []

    sensi_max_err = []
    sensi_min_err = []
    speci_max_err = []
    speci_min_err = []
    sensi = []
    sensi_err = []
    speci = []
    speci_err = []

    #################### variable pour les zones
    vline_emitted = []
    vline_detected = []
    vline_both = []
    bool_emitted = False
    bool_detected = False

    for packet in shark_cap: 
        detect_packet = ""
        src_ip, dst_ip, src_port, dst_port = utils.get_packet_info(packet)

        paquet += 1
        num_paquet_total += 1

        #################### Marquage des paquets
        if(len(attack_paquet) > 0):
            fst_packet = attack_paquet[0]
            # print("src_ip:", src_ip)
            # print("fst_packet[0]", fst_packet[0])
            if(src_ip == fst_packet[0] and src_port == fst_packet[1] and dst_ip == fst_packet[2] and dst_port == fst_packet[3]):
                attack_paquet.pop(0)
                # detect_packet += "Paquet Attaquant\n"
                detect_packet += "1"
                paquet_ddos += 1
                bool_emitted = True
            else:
                # detect_packet += "Paquet Legit\n"
                detect_packet += "2"
                paquet_legit += 1
        else:
            # detect_packet += "Paquet Legit\n"
            detect_packet += "2"
            paquet_legit += 1
    
        if(num_paquet_total in filtre_paquet_ddos):
            # detect_packet += "Marqué Attaquant" 
            detect_packet += "a"
            bool_detected = True
        else:
            # detect_packet += "Marqué Legit"
            detect_packet += "b"

        if(detect_packet == "1a"): vrai_positif += 1
        elif(detect_packet == "1b"): faux_negatif += 1
        elif(detect_packet == "2a"): faux_positif += 1
        elif(detect_packet == "2b"): vrai_negatif += 1

        if(vrai_positif > 0 and faux_negatif > 0):
            sensibilite = vrai_positif/(vrai_positif+faux_negatif)
        else:
            sensibilite = 1.0

        if(vrai_negatif > 0 and faux_positif > 0):
            specificite = vrai_negatif/(vrai_negatif+faux_positif) 
        else:
            specificite = 1.0

        sensi_temp.append(sensibilite)
        speci_temp.append(specificite)

        seconde = packet.sniff_timestamp.split(".")[0]
        if(fst_timestamp == 0):
             fst_timestamp = seconde

        # seconde_exact
        # seconde_exact = int(seconde)-int(fst_timestamp)

        # intervalle de 15 secondes
        seconde_exact = (int(seconde)-int(fst_timestamp))//15

        #Permet de marquer les zones d'attaque émises et/ou détecté 
        if(bool_emitted and not bool_detected):
            bool_emitted = False
            if(seconde_exact not in vline_emitted):
                vline_emitted.append(seconde_exact)
        elif(bool_detected and not bool_emitted):
            bool_detected = False
            if(seconde_exact not in vline_detected):
                vline_detected.append(seconde_exact)
        elif(bool_emitted and bool_detected):
            bool_emitted = False
            bool_detected = False
            if(seconde_exact not in vline_both):
                vline_both.append(seconde_exact)

            
        if(seconde_exact > seconde_temp):
            if(len(sensi_temp) > 0 and len(speci_temp) > 0):
                avg_sensi = np.average(sensi_temp)
                max_sensi = max(sensi_temp)
                min_sensi = min(sensi_temp)

                avg_speci = np.average(speci_temp)
                max_speci = max(speci_temp)
                min_speci = min(speci_temp)

                sensi_temp.clear()
                speci_temp.clear()

                sensi.append((seconde_exact, avg_sensi))
                sensi_max_err.append(max_sensi - avg_sensi)
                sensi_min_err.append(avg_sensi - min_sensi)
                speci.append((seconde_exact, avg_speci))
                speci_max_err.append(max_speci - avg_speci)
                speci_min_err.append(avg_speci - min_speci)

                # sensi_max_display.append((seconde_exact, max_sensi))
                # sensi_min_display.append((seconde_exact, min_sensi))
                # speci_max_display.append((seconde_exact, max_speci))
                # speci_min_display.append((seconde_exact, min_speci))

                vrai_positif = 0
                vrai_negatif = 0
                faux_positif = 0
                faux_negatif = 0

            nb_paquet.append(paquet)
            paquet = 0

            nb_paquet_ddos.append(paquet_ddos)
            paquet_ddos = 0
        
            nb_paquet_legit.append(paquet_legit)
            paquet_legit = 0

            if(seconde_exact-seconde_temp>1):
                for temp in range(seconde_temp+1, seconde_exact):
                    nb_paquet.append(0)
                    nb_paquet_ddos.append(0)
                    nb_paquet_legit.append(0)
                    
            seconde_temp = seconde_exact

    # print("nb_paquet_ddos: ", nb_paquet_ddos)
    # print("total: ", sum(nb_paquet_ddos))
    list_minutes = range(0, len(nb_paquet))
    list_minutes_tick = range(0,len(nb_paquet), round(len(nb_paquet)*0.05))
    list_minutes_label = [item * 15 for item in list_minutes_tick]
    sensi_err = [sensi_min_err, sensi_max_err]
    speci_err = [speci_min_err, speci_max_err]

    fig, ax = plt.subplots(figsize=(20, 10))  

    ax.plot(list_minutes, nb_paquet, "blue", label="Tous les paquets")  
    ax.plot(list_minutes, nb_paquet_ddos, "red", label="Paquets DDoS", linestyle=":")
    ax.plot(list_minutes, nb_paquet_legit, "limegreen", label="Paquets légitimes", linestyle=":")
    ax.legend(bbox_to_anchor=(-0.04,1))
    ax.set_title("Paquets par groupe de 15 secondes")
        
    plt.xlabel("Secondes")
    plt.xticks(ticks=list_minutes_tick, labels=list_minutes_label)
    plt.ylabel("Nombre de paquets")
    
    ax2 = ax.twinx()

    ax2.vlines(vline_emitted, ymin=0.05, ymax=0.95, colors='lightcoral', ls="dotted", label='Zone de paquets ddos émis')
    ax2.vlines(vline_detected, ymin=0.05, ymax=0.95, colors='cyan', ls="dotted", label='Zone de paquets ddos détecté')
    ax2.vlines(vline_both, ymin=0.05, ymax=0.95, colors='lawngreen', ls="dotted", label='Zone de paquets ddos émis et détecté')

    sensi_markers, sensi_caps, sensi_bars = ax2.errorbar([i[0] for i in sensi], [i[1] for i in sensi], yerr=sensi_err, label="Sensibilité", fmt='o', color='orange', elinewidth=0.7, markersize='1', capsize=2)    
    speci_markers, speci_caps, speci_bars = ax2.errorbar([i[0] for i in speci], [i[1] for i in speci], yerr=speci_err, label="Spécificité", fmt='o', color='blue', elinewidth=0.7, markersize='1', capsize=2)
    
    ################## TRANSPARENCE ##################
    # [bar.set_alpha(0.4) for bar in sensi_bars]
    # [cap.set_alpha(0.4) for cap in sensi_caps]
    [bar.set_alpha(0.4) for bar in speci_bars]
    [cap.set_alpha(0.4) for cap in speci_caps]

    # ax2.plot([i[0] for i in sensi_max_display], [i[1] for i in sensi_max_display], label="Sensibilité cumulé max")
    # ax2.plot([i[0] for i in sensi_min_display], [i[1] for i in sensi_min_display], label="Sensibilité cumulé min")
    # ax2.plot([i[0] for i in speci_max_display], [i[1] for i in speci_max_display], label="Spécificité cumulé max")
    # ax2.plot([i[0] for i in speci_min_display], [i[1] for i in speci_min_display], label="Spécificité cumulé min")

    # ax2.scatter([i[0] for i in sensi_max_display], [i[1] for i in sensi_max_display], label="Sensibilité cumulé max", s=0.5)
    # ax2.scatter([i[0] for i in sensi_min_display], [i[1] for i in sensi_min_display], label="Sensibilité cumulé min", s=0.5)
    # ax2.scatter([i[0] for i in speci_max_display], [i[1] for i in speci_max_display], label="Spécificité cumulé max", s=0.5)
    # ax2.scatter([i[0] for i in speci_min_display], [i[1] for i in speci_min_display], label="Spécificité cumulé min", s=0.5)

    ax2.legend(bbox_to_anchor=(1.04,1))
    plt.ylabel("Taux sensibilité/spécificité")
    plt.savefig('paquets_secondes.svg', bbox_inches="tight")



def pie(result):
    """ 
    Fonction permettant de créer le graph paquets par secondes 
    """
    fig = plt.figure()
    fig.savefig("pie.svg")

    my_json = open(result)
    data = json.load(my_json)
    resultat = data.get("Résultats")

    vrai_negatif = resultat[0].get("vrai_negatif")
    faux_positif = resultat[0].get("faux_positif")
    faux_negatif = resultat[0].get("faux_negatif")
    vrai_positif = resultat[0].get("vrai_positif")

    fig = plt.figure(figsize=(8, 6)) 
    grid = GridSpec(3, 2) 

    #Matrice
    ax1 = plt.subplot(grid[0, 0])    
    columns = ('Paquets DDoS', 'Paquets légitimes')
    rows = ('Nombre paquets marqué', 'Nombre paquets pas marqué')

    cell_text = [["Vrai positif: "+str(vrai_positif), "Faux positif: "+str(faux_positif)],
                ["Faux négatif: "+str(faux_negatif), "Vrai négatif: "+str(vrai_negatif)]]
    ax1.axis('off')
    ax1.axis('tight')
    table = ax1.table(cellText=cell_text, rowLabels=rows, colLabels=columns, loc='center', cellLoc='center')
    table.auto_set_font_size(True)
    table.scale(0.6, 0.6)  # may help
    ax1.set_title("Matrice de confusion", fontsize=6)

    #Camembert avec tout les paquets
    ax2 = plt.subplot(grid[1, 0])
    legend = ["Vrai négatif (paquets legitime pas marqués)",
                "Faux positif (paquets legitime marqués)", 
                "Faux negatif (paquets attaquant pas marqués)", 
                "Vrai positif (paquets attaquant marqués)"]
    packet = [vrai_negatif, faux_positif, faux_negatif, vrai_positif]
    colors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral']
    wedges, texts, autotexts = ax2.pie(packet, colors=colors, radius=3, center=(4, 4),
        wedgeprops={"linewidth": 1, "edgecolor": "white"}, frame=True, 
        autopct=utils.make_autopct(packet), textprops={'fontsize': 3})
    ax2.axis('off')
    ax2.legend(wedges, legend, prop={'size': 3})
    ax2.set_title("Proportion des paquets", fontsize=6)

    #Camembert avec les paquets légitimes
    ax3 = plt.subplot(grid[0, 1])
    legend = ["Vrai negatif (paquets legitime pas marqués)", 
                "Faux positif (paquets legitime marqués)"]
    packet = [vrai_negatif, faux_positif]
    colors = ['yellowgreen', 'gold']
    wedges, texts, autotexts = ax3.pie(packet, colors=colors, radius=3, center=(4, 4),
        wedgeprops={"linewidth": 1, "edgecolor": "white"}, frame=True, 
        autopct=utils.make_autopct(packet), textprops={'fontsize': 3})
    ax3.axis('off')
    ax3.legend(wedges, legend, loc="best", prop={'size': 3})
    ax3.set_title("Proportion de paquets mal détecté parmi légitime (spécificité)", fontsize=6)

    #Camembert avec les paquets attaquants
    ax4 = plt.subplot(grid[1, 1])
    legend = ["Faux negatif (paquets attaquants pas marqué)",
                "Vrai positif (paquets attaquants marqué)"]
    packet = [faux_negatif, vrai_positif]
    colors = ['lightskyblue', 'lightcoral']
    wedges, texts, autotexts = ax4.pie(packet, colors=colors, radius=3, center=(4, 4),
        wedgeprops={"linewidth": 1, "edgecolor": "white"}, frame=True, 
        autopct=utils.make_autopct(packet), textprops={'fontsize': 3})
    ax4.axis('off')
    ax4.legend(wedges, legend, loc="best", prop={'size': 3})
    ax4.set_title("Proportion de paquets mal détecté parmi malveillant (sensibilité)", fontsize=6)

    fig.tight_layout()
    plt.axis('off')
    plt.savefig("pie.svg", bbox_inches='tight')


def main():
    parser = ArgumentParser()
    parser.add_argument('--attack', help='Fichier attaque de la capture')
    parser.add_argument('--filtre', help='Fichier résultant du filtre mis en place')
    parser.add_argument('--pcap', help='Fichier pcap de la capture')    
    parser.add_argument('--result', help='Fichier résultat')    
    parser.add_argument("--packet_seconds", '-ps', action='store_true', help="Création du graphe paquets/minutes")
    parser.add_argument("--pie", '-p', action='store_true', help="Création des camemberts avec la matrice de confusion")

    args = parser.parse_args()
    
    if args.pie is not None or args.packet_seconds is not None: 
        print("\Création des graphes veuillez patienter")
        print("-----------------------------------------------------")

        if args.packet_seconds:
            print("-------------------  Graphes Packets/seconds  --------------------")
            paquets_minutes(args.filtre, args.pcap, args.attack)
            print("---------------------------------------\n")

        if args.pie:
            print("-------------------  Graphes pie + matrice  --------------------")
            pie(args.result) 
            print("---------------------------------------\n")
            
        print("-----------------------------------------------------")
        print("Fin")
    else:
        parser.print_help()


if __name__ == '__main__':
    main()