#!/bin/bash 

#Permet de générer les fichiers graph de toutes les captures déjà présentes dans le repot selon le filtre
if [[ $1 == "1" ]]
then
    clear
    echo "------------------------------------"
    echo "Génération graph"
    echo "------------------------------------"
    cd Captures/Capture0
    echo "- Capture0"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture1
    echo "- Capture1"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture2
    echo "- Capture2"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture3
    echo "- Capture3"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture4
    echo "- Capture4"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture5
    echo "- Capture5"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture6
    echo "- Capture6"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture7
    echo "- Capture7"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture8
    echo "- Capture8"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
    cd ../Capture9
    echo "- Capture9"
    python3 ../../graph.py --filtre DDoS-Detect_TRICHE.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_detect.json -p
    echo "------------------------------------"
elif [[ $1 == "2" ]]
then
    clear
    echo "------------------------------------"
    echo "Génération graph"
    echo "------------------------------------"
    cd Captures/Capture0
    echo "- Capture0"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture1
    echo "- Capture1"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture2
    echo "- Capture2"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture3
    echo "- Capture3"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture4
    echo "- Capture4"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture5
    echo "- Capture5"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture6
    echo "- Capture6"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture7
    echo "- Capture7"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture8
    echo "- Capture8"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
    cd ../Capture9
    echo "- Capture9"
    python3 ../../graph.py --filtre DDoS-Attack-Detection-System.txt --pcap capture.pcap --attack list_packet_attack.log -ps --result result_ddos_attack_detection.json -p    
    echo "------------------------------------"
else
    echo "------------------------------------"
    echo
    echo "Les deux graphes seront générés pour toutes les captures"
    echo
    echo "Arguments possibles:"
    echo "   \"1\"      [filtre 1: ddos_detect]"
    echo "   \"2\"      [filtre 2: ddos_attack_detection]"
    echo 
    echo " /!\ Attention a bien changer les import dans \"utils.py\" /!\ "
    echo
    echo "------------------------------------"
fi
