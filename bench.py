#!/usr/bin/env python3

import pyshark
import json
import utils
from argparse import ArgumentParser

def analyse(log, filtre, pcap, attack, verbose):

    ##### Variable pour fichier log
    log_nb_attaque = 0
    log_ip = 0
    log_attaque = []

    ##### Variable pour fichier attack
    attack_paquet = []
    attack_nb_paquet_ddos = 0

    ##### Variable pour fichier filtre
    filtre_nb_paquet_ddos = 0
    filtre_paquet_ddos = []

    ##### Variable pour la capture
    pcap_nb_paquet_ddos = 0
    pcap_paquet_ddos = []

    tpr = 0
    tnr = 0

    ######################################### LECTURE FICHIER LOG ######################################### 
    log_file = open(log, 'r')
    Lines = log_file.readlines()
    for line in Lines:
        word = line.split(":")
        if(word[0] == "Nb_attaque"):
            log_nb_attaque = str(word[1])
        elif(word[0] == "IP"):
            log_ip = str(word[1])
        elif(word[0] == "Attaque"):
            args = word[ len(word)-2 ]
            args_split = args.split(",")
            duree = args_split[len(args_split)-1][1:-1]
            log_attaque.append([word[3], duree])
    
    if(verbose):
        print("Attaque: [[Début, Durée], ...]")
        print(log_attaque)

    log_file.close()

    ######################################### LECTURE FICHIER ATTACK ######################################### 
    log_file2 = open(attack, 'r')
    Lines = log_file2.readlines()
    for line in Lines:
        word = line.split(";")
        #IP_source, PORT_source, IP_dest, POST_dest
        attack_paquet.append([word[0], word[1], word[2], word[3]])
        attack_nb_paquet_ddos += 1

    log_file2.close()

    ######################################### LECTURE FICHIER FILTRE #########################################
    reader = utils.ids_reader(filtre)
    filtre_nb_paquet_ddos, filtre_paquet_ddos = reader.read()


    ######################################### LECTURE FICHIER PCAP #########################################
    shark_cap = pyshark.FileCapture(pcap)

    num_paquet = 0
    for packet in shark_cap:
        num_paquet += 1
        src_ip = None
        dst_ip = None
        src_port = None
        dst_port = None

        if("IPV6" in str(packet.layers)):
            src_ip = packet.ipv6.src
            dst_ip = packet.ipv6.dst
        elif("IP" in str(packet.layers)):
            src_ip = packet.ip.src
            dst_ip = packet.ip.dst
        
        if("UDP" in str(packet.layers)):
            src_port = packet.udp.srcport
            dst_port = packet.udp.dstport
        elif("TCP" in str(packet.layers)):
            src_port = packet.tcp.srcport
            dst_port = packet.tcp.dstport


        # if(verbose):
        #     print("num paquet:" , num_paquet)

        if(len(attack_paquet) > 0):
            fst_packet = attack_paquet[0]
            if(src_ip == fst_packet[0] and src_port == fst_packet[1] and dst_ip == fst_packet[2] and dst_port == fst_packet[3]):
                attack_paquet.pop(0)
                pcap_paquet_ddos.append(num_paquet)
                pcap_nb_paquet_ddos += 1
                if(verbose):
                    print("ATTAQUE ---------------------------------------------------------")
                    print(" Nombre paquets trouvés: ", pcap_nb_paquet_ddos)
                    print(" Nombre paquets DDOS restants: " , len(attack_paquet))
                    print("---------------------------------------------------------")
                    print("IP: src", src_ip, " IP: paquet ", fst_packet[0])
                    print("port: src", src_port, " port: paquet ", fst_packet[1])
                    print("IP: dst", dst_ip, " IP: paquet ", fst_packet[2])
                    print("port: dst", dst_port, " port: paquet ", fst_packet[3])
                    print("---------------------------------------------------------\n\n")


    ######################################### FIN LECTURE FICHIERS #########################################

    vrai_positif = 0
    vrai_negatif = 0
    faux_positif = 0
    faux_negatif = 0

    for i in range(num_paquet):
        if (i in filtre_paquet_ddos) and (i in pcap_paquet_ddos):
            vrai_positif += 1
        elif not (i in filtre_paquet_ddos) and not (i in pcap_paquet_ddos):
            vrai_negatif += 1
        elif (i in filtre_paquet_ddos) and (not i in pcap_paquet_ddos):
            faux_positif += 1
        elif (not i in filtre_paquet_ddos) and (i in pcap_paquet_ddos):
            faux_negatif += 1

    tpr = vrai_positif / (vrai_positif + faux_negatif)
    tnr = vrai_negatif / (vrai_negatif + faux_positif)
    accuracy = (vrai_positif + vrai_negatif) / (vrai_positif + vrai_negatif + faux_positif + faux_negatif)

    print("\n\nRésultats:")
    print(" vrai_positif: " + str(vrai_positif) + " (Vrai paquet DDOS)")
    print(" vrai_negatif: " + str(vrai_negatif) + " (Vrai paquet légitime)")
    print(" faux_positif: " + str(faux_positif) + " (Faux paquet DDOS)")
    print(" faux_negatif: " + str(faux_negatif) + " (Faux paquet légitime)")
    print(" Total: " + str(vrai_positif + vrai_negatif + faux_positif + faux_negatif))
    print("\n-----------------------------------------------------\n")
    print("Formules:")
    print(" True positive rate: (vrai_positif / (vrai_positif + faux_negatif))")
    print(" True negative rate: (vrai_negatif / (vrai_negatif + faux_positif))")
    print(" Précision:  ((vrai_positif + vrai_negatif) / (vrai_positif + vrai_negatif + faux_positif + faux_negatif))")
    print("-----------------------------------------------------")
    print(" True positive rate: " + str(tpr*100) + " %")
    print(" True negative rate: " + str(tnr*100) + " %")
    print(" Précision: " + str(accuracy*100) + " %")
    print("\n-----------------------------------------------------\n")
    print("Autres")
    print(" Nombre de paquets total: " + str(num_paquet) + " ")
    print(" Nombre de paquets ddos trouvés par le filtre: " + str(filtre_nb_paquet_ddos) + " ")
    print(" Nombre de paquets ddos trouvés dans la capture: " + str(pcap_nb_paquet_ddos) + " ")
    print(" Nombre de paquets ddos dans le fichier d'attaque: " + str(attack_nb_paquet_ddos) + " ")
    print(" Nombre d'attaque de départ: " + str(log_nb_attaque))
    # if(verbose): print("\n\nListe des numéros des paquets: " + str(pcap_paquet_ddos))



    ######################################### ECRITURE RESULT #########################################
    data = {'Scenario': 
            [
                {
                    "IP attaqué": log_ip,
                    "Nombre d'attaque départ": log_nb_attaque,
                    "Packets à filtrer comme DDoS": attack_nb_paquet_ddos,
                    "Filtre": reader.get_filter_name()
                }
            ],
            'Résultats':
            [ 
                {
                    'Packets identifié comme DDoS par le filtre': filtre_nb_paquet_ddos,
                    'vrai_positif': vrai_positif,
                    'vrai_negatif': vrai_negatif,
                    'faux_positif': faux_positif,
                    'faux_negatif': faux_negatif,
                    'True Positive Rate': tpr,
                    'True Negative Rate': tnr,
                    'Accuracy': accuracy             
                }
            ]
    }

    path = './result_' + reader.get_filter_name() + '.json'
    with open(path, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    ######################################### FIN ECRITURE RESULT #########################################


    path2 = './result_packet_' + reader.get_filter_name() + '.log'
    attack_file = open(path2,'w')

    for atk in filtre_paquet_ddos:
        attack_file.write(str(atk) + "\n")

    attack_file.close()
    shark_cap.close()
    return True


def main():
    parser = ArgumentParser()
    parser.add_argument('--log', help='Fichier log de la capture')
    parser.add_argument('--attack', help='Fichier attaque de la capture')
    parser.add_argument('--filtre', help='Fichier résultant du filtre mis en place')
    parser.add_argument('--pcap', help='Fichier pcap de la capture')
    parser.add_argument("--verbose", '-v', action='store_true', help="Mode verbose")

    args = parser.parse_args()
    
    if args.log is not None and args.filtre is not None and args.pcap is not None: 
        print("\nLecture des fichiers veuillez patienter")
        print("-----------------------------------------------------")
        analyse(args.log, args.filtre, args.pcap, args.attack, args.verbose)
        print("-----------------------------------------------------")
        print("Fin lecture")
    else:
        parser.print_help()


if __name__ == '__main__':
    main()